/****************************************************************************
*
*                                 M U E S L I   v 1.8
*
*
*     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "muesli/muesli.h"



int main(int argc, char **argv)
{
    const double E = 200.0e9;
    const double nu = 0.3;
    const double rho = 1.0;
    const std::string namenh = "a classic Neo-Hookean material";

    muesli::neohookeanMaterial matNH(namenh, E, nu, rho);
    muesli::finiteStrainMP* pNH = matNH.createMaterialPoint();

    // build NNet
    const std::string filename = "nnarchitecture.dat";
    const std::string namenn1 = "a NeuralNetwork material";
    muesli::nnFsolid matNN1(namenn1, filename);
    muesli::finiteStrainMP* pNN1 = matNN1.createMaterialPoint();
    std::ofstream outfileMP("NNMPinfo.txt");
    matNN1.print(outfileMP);

    itensor F; F = F.identity();
    double a, amax = 0.02;
    unsigned npercycle = 40;
    unsigned ncycles   = 1;


    // test 1
    /* F = [1+a 0  0       C = [(1+a)^2 0 0
             0  1  0  -->          0    1 0
             0  0  1]              0    0 1]*/

    std::ofstream os1("test1.data");
    os1 << "  a-amax        energy NH     energy NN     error"
        << "\n---------------------------------------------------";
    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2.0;
          F(0,0) = 1.0 - amax + a;
          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  <<
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */

          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy();
          istensor S_NH, S_NH_num;
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy();
          pNN1->commitCurrentState();
          os1 << "\n" << std::setw(8) << std::scientific << a - amax
              << " "  << energyNH << " " << energyNN1
              << " " << energyNH - energyNN1;
      }
    os1.close();


/****** Problema 2: ******/
  /* F = [1+a 0  0       C = [(1+a)^2    0    0
           0 1+a 0  -->          0    (1+a)^2 0
           0  0  1]              0       0    1]*/

  /**** Output file  ****/
  std::ofstream os2("test2.data");

  // double acumSqDiff = 0, acumSqEner = 0;

  F = F.identity();

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,0) = F(1,1) = (1 - amax + a);

          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  <<
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */

          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy();
          istensor S_NH, S_NH_num;
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy();
          //istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          pNN1->commitCurrentState();

          os2 << "\n" << std::setw(8) << std::scientific << -amax + a << " "  << energyNH << " "  << energyNN1;
      }

    os2.close();

/****** Problema 3: ******/
  /* F = [1 a 0       C = [1   a   0
          0 1 0  -->       a 1+a^2 0
          0 0 1]           0   0   1]*/

  /**** Output file  ****/
  std::ofstream os3("test3.data");

  // double acumSqDiff = 0, acumSqEner = 0;
  F = F.identity();

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,1) = - amax + a;

          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  <<
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */


          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy();
          istensor S_NH, S_NH_num;
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy();
          //istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          pNN1->commitCurrentState();

          os3 << "\n" << std::setw(8) << std::scientific << - amax + a << " "  << energyNH << " "  << energyNN1;
      }

    os3.close();

/****** Problema 4: ******/
  /* F = [ 1  -a   0       C = [1+a^2    -a+a^2      a-a^2
           0  1+a 2*a  -->     -a+a^2  1+2a+3a^2    3a+a^2
           a   a  1-a]          a-a^2    3a+a^2   1-2a+5a^2]*/

  /**** Output file  ****/
  std::ofstream os4("test4.data");

  // double acumSqDiff = 0, acumSqEner = 0;
  F = F.identity();

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,1) = -(-amax+a); F(1,1) = (1+(-amax+a)); F(1,2) = 2*(-amax+a);
          F(2,0) = F(2,1) = (-amax+a); F(2,2) = 1-(-amax+a);

          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  <<
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */

          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy();
          istensor S_NH, S_NH_num;
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy();
          //istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          pNN1->commitCurrentState();

          os4 << "\n" << std::setw(8) << std::scientific << (-amax+a) << " "  << energyNH << " "  << energyNN1;
      }

    os4.close();


delete pNH;
delete pNN1;

//os.close();

return 0;
}

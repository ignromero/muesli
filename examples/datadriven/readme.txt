Test of NNet-based hyperelastic material
Prepared by Néstor Cabral, Jan 2023

A NNet has been prepared to reproduce the stored energy function of a
Neo-Hookean material as a function of the deformation gradient. The
description of the NNet is in the file nnarchitecture.dat, using a
simple format that is employed in Muesli.

The test creates a NNet using the information on the description file
and runs several tests to compare it with the real Neo-Hookean response.

Usage:
 - make: build the executable file
 - make test: run the tests
 - make clean: remove all intermediate files

set style line 1 lt 1
set style line 1 lt rgb '#A00000' lw 2 pt 1
set style line 8 lc rgb '#c0c0c0' lt 1 lw 0.5;
set grid mxtics mytics linestyle 8;
#
#
# Standard output for metapost
set terminal mp color;
set output 'example.mp';
set ylabel '${\sigma_{12}/Y_0}$';
set xlabel '$\epsilon_{12}$';
#set xrange [0:10];
#set yrange [0:1.3];
set grid xtics linestyle 8;
set grid ytics linestyle 8;
set grid mxtics linestyle 8;
set grid mytics linestyle 8;

plot 'cyclictest.data' using ($2):($3/100e6) w linespoints title '' linestyle 1;

set ylabel '$\gamma$';
plot 'cyclictest.data' using ($2): ($4) w linespoints title '' linestyle 1;

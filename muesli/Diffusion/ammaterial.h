/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#ifndef _muesli_ammaterial_h
#define _muesli_ammaterial_h

#include "muesli/Diffusion/conductor.h"
#include "muesli/material.h"
#include "muesli/tensor.h"
#include <iostream>

namespace muesli
{
    class conductorMP;
    class smallStrainMaterial;
    class smallStrainMP;
    class sThermoMechMP;
    class additiveManufacturingMP;

    // AMConductorMaterial is a material class introduced for the modelling of additive manufacturing.
    // The conductivity, the heat capacity, and the density are scalar function piecewise linear and state-dependent.
    // The material is temperature dependent and has three states: Powder, Fluid, and Solid.
    // The heat capacity of material increases significantly (according to the results from CalPhaD)
    // by increasing the temperature more than boiling temperature of the material to prevent
    // increasing the temperature much higher than boiling temperature.
    class AMConductorMaterial : public muesli::conductorMaterial
    {
    public:
                                    AMConductorMaterial(const std::string& name);
                                    AMConductorMaterial(const std::string& name,
                                                        const materialProperties& cl);
                                    AMConductorMaterial(const std::string& name,
                                                 const double rho,
                                                 const double phi_,
                                                 const double d_,
                                                 const double tL_,
                                                 const double tS_,
                                                 const double tB_,
                                                 const double* k,
                                                 const double* c,
                                                 const double* r);
                                    AMConductorMaterial(const AMConductorMaterial* nlCM);


        virtual                     ~AMConductorMaterial() {}

        virtual bool                check() const;
        virtual conductorMP*        createMaterialPoint() const;
        virtual double              getProperty(const propertyName p) const;
        virtual void                print(std::ostream &of=std::cout) const;
        virtual void                setRandom();
        virtual bool                test(std::ostream& os=std::cout);

    private:
        std::vector<double>         kk, cc, rr;
        double                      tS, tL, tB, phi, d;
        friend class                AMConductorMP;
    };




    class AMConductorMP : public muesli::conductorMP
    {

    public:
                                    AMConductorMP(const AMConductorMaterial& mat);

        virtual double              density() const;
        virtual double              thermalEnergy() const;
        virtual void                heatflux(ivector &q) const;
        virtual void                heatfluxDerivative(ivector &qprime) const;

        virtual void                conductivity(istensor& K) const;
        virtual double              heatCapacity() const;
        virtual double              heatCapacityDerivative() const;


        virtual void                contractTangent(const ivector& na,
                                                    const ivector& nb, double& tg) const;
        virtual double              phase() const;

        virtual void                setRandom();
        const conductorMaterial*    getMaterial(){return mat;}

        // updates
        virtual materialState       getConvergedState() const;
        virtual materialState       getCurrentState() const;
        virtual void                updateCurrentState(double theTime, double temp, const ivector& gradT);

    private:
        enum AMphase{powder, fluid, solid};
        AMConductorMaterial const*  mat;
        AMphase                     thePhase;
    };




// small strain thermomechanic constitutive model for Additive Manufacturing applications
class additiveManufacturingMaterial : public muesli::material
{
public:
                            additiveManufacturingMaterial(const std::string& name,
                                                          const materialProperties& cl);

    virtual                 ~additiveManufacturingMaterial();

    virtual bool            check() const;
    virtual additiveManufacturingMP*  createMaterialPoint() const;
    virtual double          density() const;
    virtual double          getProperty(const propertyName p) const;
    virtual void            print(std::ostream &of=std::cout) const;
    double                  referenceTemperature() const;
    virtual void            setRandom();
    virtual bool            test(std::ostream& of=std::cout);
    virtual double          waveVelocity() const;


private:
    smallStrainMaterial     *theSSMaterial;
    double                  alpha;
    double                  conductivity;
    double                  heat_capacity;
    double                  taylor_quinney;
    double                  tref;
    double                  tS, tL;

    friend class            additiveManufacturingMP;
};

inline double additiveManufacturingMaterial::referenceTemperature() const {return tref;}




class additiveManufacturingMP : public muesli::materialPoint
{

public:
                            additiveManufacturingMP(const additiveManufacturingMaterial &m);
    virtual                 ~additiveManufacturingMP();
    virtual void            setRandom();
    bool                    testImplementation(std::ostream& of=std::cout) const;


    // energies
    virtual double          freeEnergy() const;
    virtual double          deviatoricEnergy() const;
    virtual double          dissipatedEnergy() const;
    virtual double          effectiveStoredEnergy() const;
    virtual double          volumetricEnergy() const;


    // gradients
    virtual double          entropy() const;
    virtual ivector         heatflux() const;
    virtual double          pressure() const;
    virtual void            stress(istensor& sigma) const;
    virtual void            stressTemperatureTensor(istensor& M) const;
    virtual void            deviatoricStress(istensor& s) const;
    virtual double          plasticSlip() const;


    // tangents
    virtual double          contractWithConductivity(const ivector &v1, const ivector &v2) const;
    virtual void            contractWithTangent(const ivector &v1,
                                                const ivector &v2,
                                                itensor &T) const;
    virtual void            contractWithDeviatoricTangent(const ivector &v1,
                                                          const ivector &v2,
                                                          itensor &T) const;
    virtual void            dissipationTangent(itensor4& D) const; // II - Cinverse:Cep for small sThermoMech element
    virtual void            tangentElasticities(itensor4& c) const;
    virtual double          volumetricStiffness() const;



    // bookkeeping
    virtual istensor        getConvergedPlasticStrain() const;
    virtual materialState   getConvergedState() const;
    virtual istensor        getCurrentPlasticStrain() const;
    virtual materialState   getCurrentState() const;
    istensor&               getCurrentStrain();
    const istensor&         getCurrentStrain() const;
    virtual double          phase() const;
    virtual void            updateCurrentState(const double t, const istensor& strain, const double temp, const ivector& gradt);
    virtual void            commitCurrentState();
    virtual void            resetCurrentState();
    double&                 temperature();
    const double&           temperature() const;


    double                  density() const;
    const additiveManufacturingMaterial& parentMaterial() const;


private:

    enum AMphase {powder, fluid, solid, meltedSolid, solidAgain};

    double                        dtemp_n, dtemp_c;
    ivector                       gradt_n, gradt_c;
    smallStrainMP*                theSSMP;
    additiveManufacturingMaterial const*  mat;
    AMphase                       thePhase;
};

inline double additiveManufacturingMP::density() const  {return mat->density();}
inline const additiveManufacturingMaterial& additiveManufacturingMP::parentMaterial() const {return *mat;}
}

#endif

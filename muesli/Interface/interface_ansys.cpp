/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#include "interface_ansys.h"
#include "muesli/muesli.h"

using namespace muesli;

// ANSYS
//
// input arguments
//
// matId  (int,sc,i)                        material #
// elemId (int,sc,i)                        element #
// kDomIntPt    (int,sc,i)                  kth domain integration point
// kLayer       (int,sc,i)                  kth layer
// kSectPt      (int,sc,i)                  kth Section point
// ldstep       (int,sc,i)                  load step number
// isubst       (int,sc,i)                  substep number
// nDirect      (int,sc,in)                 # of direct components
// nShear       (int,sc,in)                 # of shear components
// ncomp        (int,sc,in)                 nDirect + nShear
// nustatev      (int,sc,l)                  Number of state variables
// nProp        (int,sc,l)                  Number of material constants
//
// Temp         (dp,sc,in)                  temperature at beginning of time increment
// dTemp        (dp,sc,in)                  temperature increment
// Time         (dp,sc,in)                  current time
// dTime        (dp,sc,in)                  current time increment
//
// Strain       (dp,ar(comp),i)             Strain at beginning of time increment
// dStrain      (dp,ar(comp),i)             Strain increment
// prop         (dp,ar(comp),i)             Material constants defined by TB,USER
// coords       (dp,ar(3),i)                current coordinates
// defGrad_t    (dp,ar(3,3),i)              Deformation gradient at time t
// defGrad      (dp,ar(3,3),i)              Deformation gradient at time t+dt
//
// input output arguments
//
// stress       (dp,ar(nTesn),io)           stress
// ustatev       (dp,ar(nustatev),io)         ustatev
// sedEl        (dp,sc,io)                  elastic work
// sedPl        (dp,sc,io)                  plastic work
// epseq        (dp,sc,io)                  equivalent plastic strain
// var?         (dp,sc,io)                  not usec, they are reserved arguments for further development
//
// output arguments
//
// keycut       (int,sc,io)                 loading bisec/cut control (0 - no bisect/cut; 1 - bisect/cut). factor will be determined by ANSYS solution control.
// dsdePl       (dp,ar(ncomp,ncomp),io)     material jacobian matrix

// Voig Notation (see usermat document)
// s = [00, 11, 22, 01, 12, 02]

unsigned voigtAnsys(unsigned i, unsigned j)
{
    const unsigned voigtmap[2][6] = { {0,1,2,0,1,2}, {0,1,2,1,2,0}};
    return voigtmap[i][j];
}

static void ansysFiniteStrainTangent(const istensor& Cauchy, const itensor4& cspatial, itensor4& cansys);


int interface_ansys_(int* matlabel,
                     int* matId,
                     int* elemId,
                     int* kDomIntPt,
                     int* kLayer,
                     int* kSectPt,
                     int* ldstep,
                     int* isubst,
				     int* keycut,
                     int* nDirect,
                     int* nShear,
                     int* ncomp,
                     int* nStatev,
                     int* nProp,
                     double* Time,
                     double* dTime,
                     double* Temp,
                     double* dTemp,
                     double* stress,
                     double* ustatev,
                     double* dsdePl,
                     double* sedEl,
					 double* sedPl,
					 double* epseq,
					 double* Strain,
					 double* dStrain,
					 double* epsPl,
					 double* prop,
                     double* coords,
					 double* var0,
                     double* defGrad_t,
                     double* defGrad,
                     double* tsstif,
                     double* epsZZ,
                     double* cutFactor,
                     double* var1,
                     double* var2,
                     double* var3,
                     double* var4,  
                     double* var5,
                     double* var6,
                     double* var7  
                    )
{
    double tangent[6][6];

    switch (*matlabel)
    {
        case 1:
        {
            // linear elastic material
            //
            // matproperties[0] : E
            // matproperties[1] : nu
            const double E     = prop[1];
            const double nu    = prop[2];
            const double rho   = 1.0;
				

            elasticIsotropicMaterial *theMaterial = new elasticIsotropicMaterial("an elastic material", E, nu, rho);
            elasticIsotropicMP       *thePoint    = dynamic_cast<elasticIsotropicMP*>(theMaterial->createMaterialPoint());

            // set converged and current states
            istensor epsc(    Strain[0]+dStrain[0],      Strain[1]+dStrain[1],      Strain[2]+dStrain[2],
                          .5*(Strain[4]+dStrain[4]), .5*(Strain[5]+dStrain[5]), .5*(Strain[3]+dStrain[3]));

            double timec(*Time);
            thePoint->updateCurrentState(timec, epsc);
            thePoint->commitCurrentState();
			

            // compute all the required fields
            double stressMuesli[6];
            istensor stressT;
            thePoint->stress(stressT);
            ContraContraSymTensorToVector(stressT, stressMuesli);

            stress[0] = stressMuesli[0];
            stress[1] = stressMuesli[1];
            stress[2] = stressMuesli[2];
            stress[3] = stressMuesli[5];
            stress[4] = stressMuesli[3];
            stress[5] = stressMuesli[4];

            // tangent
            itensor4 tg;
            thePoint->tangentTensor(tg);

            for (unsigned i=0; i<6; i++)
            {
                for (unsigned j=0; j<6; j++)
                {
                    tangent[i][j] = tg(voigtAnsys(0,i), voigtAnsys(1,i), voigtAnsys(0,j), voigtAnsys(1,j));
                }
            }

            delete thePoint;
            delete theMaterial;
            
			break;

        }

        case 2:
        {
            // small strain elastoplastic material
            //
            // material constants
            //
            const double E     = prop[1];   // Young modulus
            const double nu    = prop[2];   // Poisson ratio
            const double Y0    = prop[3];   // yield stress
            const double Hi    = prop[4];   // isotropic hardening modulus
            const double Hk    = prop[5];   // kinematic hardening modulus
            const double rho   = 1.0;        // density
			
            // create material and material point
            splasticMaterial *theMaterial = new splasticMaterial("a splastic material", E, nu, rho, Hi, Hk, Y0, 0.0, "mises");
            splasticMP       *thePoint    = dynamic_cast<splasticMP*>(theMaterial->createMaterialPoint());

            // set converged states
            istensor epsn( Strain[0], Strain[1], Strain[2], .5*Strain[4], .5*Strain[5], .5*Strain[3]);
            istensor ep_n;
            double   dg_n;
            double   xi_n;
            istensor Xi_n;

            double tf(*Time);
            if ( tf == 0.0 )
            {
                ep_n.setZero();
                dg_n = 0.0;
                xi_n = 0.0;
                Xi_n.setZero();
            }
            else
            {
                istensor ep_t( ustatev[0], ustatev[1], ustatev[2], ustatev[3], ustatev[4], ustatev[5]);
                ep_n = ep_t;
                dg_n = ustatev[6];
                xi_n = ustatev[7];
                istensor Xi_t( ustatev[8], ustatev[9], ustatev[10], ustatev[11], ustatev[12], ustatev[13]);
                Xi_n = Xi_t;
            }
            // set current states
            istensor epsc(    Strain[0]+dStrain[0],      Strain[1]+dStrain[1],      Strain[2]+dStrain[2],
                          .5*(Strain[4]+dStrain[4]), .5*(Strain[5]+dStrain[5]), .5*(Strain[3]+dStrain[3]));

            double timen(*Time), timec(*Time+*dTime);


            thePoint->setConvergedState(timen, epsn, dg_n, ep_n, xi_n, Xi_n);
            thePoint->updateCurrentState(timec, epsc);

            // compute all the required fields
            double stressMuesli[6];
            istensor stressT;
            thePoint->stress(stressT);
            ContraContraSymTensorToVector(stressT, stressMuesli);

            stress[0] = stressMuesli[0];
            stress[1] = stressMuesli[1];
            stress[2] = stressMuesli[2];
            stress[3] = stressMuesli[5];
            stress[4] = stressMuesli[3];
            stress[5] = stressMuesli[4];

            // tangent
            itensor4 tg;
            thePoint->tangentTensor(tg);

            for (unsigned i=0; i<6; i++)
            {
                for (unsigned j=0; j<6; j++)
                {
                    tangent[i][j] = tg( voigtAnsys(0,i), voigtAnsys(1,i), voigtAnsys(0,j), voigtAnsys(1,j));
                }
            }

            // recover and store state variables
            double xi_c;
            istensor ep_c, Xi_c;

            materialState state_c = thePoint->getCurrentState();

            xi_c = state_c.theDouble[1];
            ep_c = state_c.theStensor[1];
            Xi_c = state_c.theStensor[2];

            // store the updated state variables
            ustatev[0] = ep_c(0,0);
            ustatev[1] = ep_c(1,1);
            ustatev[2] = ep_c(2,2);
            ustatev[3] = ep_c(1,2);
            ustatev[4] = ep_c(2,0);
            ustatev[5] = ep_c(0,1);
            //ustatev[6] = dg_c;
            ustatev[7] = xi_c;
            ustatev[8] = Xi_c(0,0);
            ustatev[9] = Xi_c(1,1);
            ustatev[10] = Xi_c(2,2);
            ustatev[11] = Xi_c(1,2);
            ustatev[12] = Xi_c(2,0);
            ustatev[13] = Xi_c(0,1);
            
            epsPl[0] = ep_c(0,0);
            epsPl[1] = ep_c(1,1);
            epsPl[2] = ep_c(2,2);
            epsPl[3] = ep_c(0,1);
            epsPl[4] = ep_c(1,2);
            epsPl[5] = ep_c(0,2);
		
            delete thePoint;
            delete theMaterial;

			break;
        }
		            

        case 3:
        {
            // finite strain neohookean
            //
            // material constants
            //
            const double E     = prop[1];   // Young modulus
            const double nu    = prop[2];   // Poisson ratio
            const double rho   = 1.0;        // density

            // create material and material point
            neohookeanMaterial *theMaterial = new neohookeanMaterial("a neohookean material", E, nu, rho);
            neohookeanMP       *thePoint    = dynamic_cast<neohookeanMP*>(theMaterial->createMaterialPoint());

            // set converged states
            itensor Fn;
            double tf(*Time);
            if (tf == 0.0)
                Fn.setZero();
            else
                muesli::vectorToTensor(defGrad_t, Fn);

            // set current state
            itensor Fc;
            muesli::vectorToTensor(defGrad, Fc);
            double timen(*Time), timec(*Time+*dTime);

            thePoint->setConvergedState(timen, Fn);
            thePoint->updateCurrentState(timec, Fc);

            // compute Cauchy stress and pass it to Ansys voigt
            istensor sigma;
            thePoint->CauchyStress(sigma);

            double stressMuesli[6];
            ContraContraSymTensorToVector(sigma, stressMuesli);
            stress[0] = stressMuesli[0];
            stress[1] = stressMuesli[1];
            stress[2] = stressMuesli[2];
            stress[3] = stressMuesli[5];
            stress[4] = stressMuesli[3];
            stress[5] = stressMuesli[4];

            // tangent
            itensor4 cspatial;
            thePoint->spatialTangent(cspatial);
            itensor4 cansys;
            ansysFiniteStrainTangent(sigma, cspatial, cansys);


            for (unsigned i=0; i<6; i++)
            {
                for (unsigned j=0; j<6; j++)
                {
                    tangent[i][j] = cansys(voigtAnsys(0,i), voigtAnsys(1,i), voigtAnsys(0,j), voigtAnsys(1,j));
                }
            }

            // recover and store state variables
            // nothing to be done
            
            delete thePoint;
            delete theMaterial;
   
			break;
	    }


        default:
		{
            for (unsigned i=0; i<6; i++)
            {
                for (unsigned j=0; j<6; j++)
                {
                    tangent[i][j] = 0.0;
                }
            }
            break;
		}
    }

    int k=0;
    for (int a=0; a< *ncomp; a++)
        for (int b=0; b< *ncomp; b++)
        {
            dsdePl[k] = tangent[a][b];
            k++;
        }

    return 0;
}




void ansysFiniteStrainTangent(const istensor& sigma, const itensor4& csp, itensor4& cansys)
{
    istensor id = istensor::identity();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    cansys(i,j,k,l) = csp(i,j,k,l) + id(i,k)*sigma(j,l) + id(j,l)*sigma(i,k);
}




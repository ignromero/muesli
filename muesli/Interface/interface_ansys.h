/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#pragma once
#ifndef _muesli_interface_ansys_
#define _muesli_interface_ansys_

extern "C"

int interface_ansys_(int* matlabel,
                     int* matId,
                     int* elemId,
                     int* kDomIntPt,
                     int* kLayer,
                     int* kSectPt,
                     int* ldstep,
                     int* isubst,
				     int* keycut,
                     int* nDirect,
                     int* nShear,
                     int* ncomp,
                     int* nStatev,
                     int* nProp,
                     double* Time,
                     double* dTime,
                     double* Temp,
                     double* dTemp,
                     double* stress,
                     double* ustatev,
                     double* dsdePl,
                     double* sedEl,
					 double* sedPl,
					 double* epseq,
					 double* Strain,
					 double* dStrain,
					 double* epsPl,
					 double* prop,
                     double* coords,
					 double* var0,
                     double* defGrad_t,
                     double* defGrad,
                     double* tsstif,
                     double* epsZZ,
                     double* cutFactor,
                     double* var1,
                     double* var2,
                     double* var3,
                     double* var4,  
                     double* var5,
                     double* var6,
                     double* var7);
#endif


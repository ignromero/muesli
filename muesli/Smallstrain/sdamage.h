/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#pragma once
#ifndef _muesli_sdamage_h
#define _muesli_sdamage_h

#include "muesli/Math/matrix.h"
#include "muesli/Math/realvector.h"
#include "smallstrain.h"
#include <iostream>


namespace muesli
{
    // Base class for isotropic small-strain elasto-plastic damage material models
    class sdamageMaterial : public muesli::smallStrainMaterial
    {

    public:
                                sdamageMaterial(const std::string& name,
                                                const double E, const double nu, const double rho,
                                                const double yield);
                                sdamageMaterial(const std::string& name,
                                                 const materialProperties& cl);

        virtual                 ~sdamageMaterial(){}

        virtual bool            check() const = 0;
        virtual smallStrainMP*  createMaterialPoint() const = 0;
        virtual double          getProperty(const propertyName p) const = 0;
        virtual void            print(std::ostream &of=std::cout) const;
        virtual void            setRandom() = 0;
        virtual bool            test(std::ostream  &of=std::cout) = 0;
        virtual double          waveVelocity() const;

    private:
        friend class                    sdamageMP;

    protected:
        double                          E, nu;          // Young's modulus and Poisson's ratio
        double                          bulk;           // bulk modulus
        double                          cp, cs;         // wave velocities
        double                          lambda, mu;     // Lamé parameters
        double                          rho;            // density
        double                          Y0;             // Yield stress
    };



    // Base class for material points of a isotropic small-strain elasto-plastic damage material models
    class sdamageMP : public muesli::smallStrainMP
    {

    public:
                                sdamageMP(const sdamageMaterial &m);
        virtual                 ~sdamageMP(){};
        virtual void            setRandom() = 0;

        // energy
        virtual double          deviatoricEnergy() const = 0; // energy for change of geometry
        virtual double          energyDissipationInStep() const = 0;
        virtual double          effectiveStoredEnergy() const = 0;
        virtual double          kineticPotential() const = 0;
        virtual double          storedEnergy() const = 0;
        virtual thPotentials    thermodynamicPotentials() const = 0;
        virtual double          volumetricEnergy() const = 0;

        // stress
        virtual void            stress(istensor& sigma) const = 0; // total stress
        virtual double          pressure() const; // hydrostatic pressure of stress state
        virtual void            deviatoricStress(istensor& s) const; // deviatoric stress //

        // tangents
        virtual void            tangentTensor(itensor4& C) const = 0; // Construct the fourth oder tensor of tangent elasticities

        // other info
        virtual double          plasticSlip() const; // return the plastic slip of the mp
        virtual materialState   getConvergedState() const ;
        virtual materialState   getCurrentState() const ;
        double                  damage() const; // return the damage of the mp

        // bookkeeping
        virtual void            commitCurrentState();
        virtual void            resetCurrentState();
        virtual void            updateCurrentState(const double theTime, const istensor& strain)=0;

    protected:
        double                  d_n;        // isotropic damage internal variable that is computed in this step --> can be recieved using the getConvergedState function
        double                  dg_n;       // delta gamma at time n
        istensor                ep_n;       // plastic deviatoric strain

        double                  d_c;        // isotropic damage internal variable of the current step
        double                  dg_c;       // delta gamma at current time
        istensor                ep_c;       // plastic deviatoric strain of current step
    };




    // Gurson–Tvergaard–Needleman material model
    class GTN_Material : public muesli::sdamageMaterial
    {
    public:
                                        GTN_Material(const std::string& name, const materialProperties& cl);

                                        GTN_Material(const std::string& name, const double E, const double nu,
                                                     const double rho, const double q1, const double q2, const double yield);

        virtual                         ~GTN_Material(){}

        virtual bool                    check() const;
        virtual muesli::smallStrainMP*  createMaterialPoint() const;
        virtual double                  getProperty(const propertyName p) const;
        virtual void                    print(std::ostream &of=std::cout) const;
        virtual void                    setRandom();
        virtual bool                    test(std::ostream  &of=std::cout);
        virtual double                  waveVelocity() const;

    private:
        friend class                    GTN_MP;
        double                          q1, q2;
    };



    class GTN_MP : public muesli::sdamageMP
    {
    public:
                                GTN_MP(const GTN_Material &m);
        virtual                 ~GTN_MP(){};
        virtual void            setRandom();

        // tangents
        virtual void            tangentTensor(itensor4& C) const;
        virtual double          shearStiffness() const;

        // energies
        virtual double          deviatoricEnergy() const;
        virtual double          energyDissipationInStep() const;
        virtual double          effectiveStoredEnergy() const;
        virtual double          kineticPotential() const;
        virtual double          storedEnergy() const;
        virtual thPotentials    thermodynamicPotentials() const;
        virtual double          volumetricEnergy() const;


        // stress
        virtual void            stress(istensor& sigma) const;
        virtual void            deviatoricStress(istensor& s) const;

        // bookkeeping
        virtual void            updateCurrentState(const double theTime, const istensor& strain);

    private:
        double                  eq_stress() const;
        double                  vol_stress() const;
        double                  vol_pslip() const;
        double                  void_fraction() const;
        const GTN_Material&     theGTNMaterial;

        void                    gursonReturn(double& dg, const istensor& strain);
        static double           fvFunction(const GTN_Material& m, const double& d, const double& t);
        static matrix           jacobianFunction(const GTN_Material& m, double *variables, const double d, const double s, const double t);
        static realvector       Newton_Raphson_System(double *variables, const double tolerance);
        static realvector       systemEqFunction(const GTN_Material& m, double *variables, const double s, const double t, const double d);
        static double           yieldfunction(const GTN_Material& m, const double& fv, const double& s, const double& t);
    };




    // Gurson material model
    /* Gurson isotropic damge material model without kinematic hardening
    The material is implement as specified in "Computational Methods for plasticity" by
    "Eduardo de Souza Neto" in box 12.6*/
    class Gurson_Material : public muesli::sdamageMaterial
    {
    public:
                                        Gurson_Material(const std::string& name, const materialProperties& cl);

                                        Gurson_Material(const std::string& name, const double E, const double nu,
                                                     const double rho, const double xRinf, const double xRb, const double yield);

        virtual                         ~Gurson_Material(){}

        virtual bool                    check() const;
        virtual muesli::smallStrainMP*  createMaterialPoint() const;
        virtual double                  getProperty(const propertyName p) const;
        virtual void                    print(std::ostream &of=std::cout) const;
        virtual void                    setRandom();
        virtual bool                    test(std::ostream  &of=std::cout);

    private:
        friend class                    Gurson_MP;
        double                          R_inf, R_b; // Hardening: sig_y(R) = sig_Y0 + R_inf (1-exp(-R_b*R))

    };



    class Gurson_MP : public muesli::sdamageMP
    {
    public:
                                Gurson_MP(const Gurson_Material &m);
        virtual                 ~Gurson_MP(){};
        virtual void            setRandom();

        // three dimensional response
        virtual void            tangentTensor(itensor4& C) const;
        virtual double          shearStiffness() const;

        // energies
        virtual double          deviatoricEnergy() const;
        virtual double          energyDissipationInStep() const;
        virtual double          effectiveStoredEnergy() const;
        virtual double          kineticPotential() const;
        virtual double          storedEnergy() const;
        virtual thPotentials    thermodynamicPotentials() const;
        virtual double          volumetricEnergy() const;


        // stress
        virtual void            stress(istensor& sigma) const;
        virtual void            deviatoricStress(istensor& s) const;

        // bookkeeping
        virtual void            updateCurrentState(const double theTime, const istensor& strain);

    private:

        double          a_fcn(const double& d_f, const double& p_f, const double& sig_yf);
        double          b_fcn(const double& p_f, const double& sig_yf);
        double          yieldstress(const double& R_f);
        double          dyieldstress(const double& R_f);
        virtual void    residual(const double& pc, const double& evetrial, const double& J2ede, realvector& res_f);
        virtual void    dresidual(const double& pc, const double& evetrial, const double& J2ede, matrix& dres_f);

        virtual void    dres_num(const double p_f, const double R_f, const double D_f, const double delgam_f, const double& evetrial, const double& J2ede, matrix& dres_f);

        const           Gurson_Material& theGursonMaterial;

        double          R_n, R_c; // Hardening variables
        matrix          Jac_c; // Final jacobian of the evolutionary equations for the computation of the stiffness matrix
    };




    /* Lemaitre-Chaboche isotropic damge material model without kinematic hardening
    The material is implement as specified in "Computational Methods for plasticity" by
    "Eduardo de Souza Neto" in box 12.4*/
    class Lemaitre_Material : public muesli::sdamageMaterial
    {
    public:
        Lemaitre_Material(const std::string& name,
                         const materialProperties& cl);

        Lemaitre_Material(const std::string& name,
                         const double E, const double nu, const double rho,
                         const double r, const double s, const double yield,
                         const double xR_inf, const double xR_b);

        virtual                         ~Lemaitre_Material(){}

        virtual bool                    check() const;
        virtual muesli::smallStrainMP*  createMaterialPoint() const;
        virtual double                  getProperty(const propertyName p) const;
        virtual void                    print(std::ostream &of=std::cout) const;
        virtual void                    setRandom();
        virtual bool                    test(std::ostream  &of=std::cout);

    private:
        friend class                    Lemaitre_MP;

        double                          r, s;       // Lemaitre Parameters
        double                          R_inf, R_b; // Hardening: sig_y(R) = sig_Y0 + R_inf (1-exp(-R_b*R))
    };



    class Lemaitre_MP : public muesli::sdamageMP
    {
    public:
                                Lemaitre_MP(const Lemaitre_Material &m);
        virtual                 ~Lemaitre_MP(){};
        virtual void            setRandom();

        // tangents
        virtual void            tangentTensor(itensor4& C) const;
        virtual double          shearStiffness() const;

        // energy
        virtual double          deviatoricEnergy() const;
        virtual double          energyDissipationInStep() const;
        virtual double          effectiveStoredEnergy() const;
        virtual double          kineticPotential() const;
        virtual double          storedEnergy() const;
        virtual thPotentials    thermodynamicPotentials() const;
        virtual double          volumetricEnergy() const;

        // stress
        void                    stress(istensor& sigma) const;
        virtual void            deviatoricStress(istensor& s) const;

        // bookkeeping
        virtual void            commitCurrentState();
        virtual void            updateCurrentState(const double theTime, const istensor& strain);

    private:
        const Lemaitre_Material& theLemaitreMaterial;
        double                  R_n, R_c;

        void                    CheckUpdateState() const; // Checks if the equilibrium equations are fulfilled
        double                  yieldfunction() const; // Compute current yield criterion
        double                  yieldstress(const double R_f) const; // Compute yield stress depending on hardening variable
        double                  dyieldstress(const double R_f) const; // Compute derivative of yield stress depending on hardening variable
    };




    /* Lemaitre-Chaboche isotropic damge material model WITH nonlinear kinematic hardening
    The material is implement as specified in "Computational Methods for plasticity" by
    "Eduardo de Souza Neto" in box 12.2*/
    class LemKin_Material : public muesli::sdamageMaterial
    {
    public:
                                        LemKin_Material(const std::string& name,
                                                         const materialProperties& cl);

                                        LemKin_Material(const std::string& name,
                                                         const double E, const double nu, const double rho,
                                                         const double r, const double s, const double yield,
                                                         const double xR_inf, const double xR_b,
                                                         const double xa, const double xb);

        virtual                         ~LemKin_Material(){}

        virtual bool                    check() const;
        virtual muesli::smallStrainMP*  createMaterialPoint() const;
        virtual double                  getProperty(const propertyName p) const;
        virtual void                    print(std::ostream &of=std::cout) const;
        virtual void                    setRandom();
        virtual bool                    test(std::ostream  &of=std::cout);

    private:
        friend class                    LemKin_MP;

        double                          r, s;       // Lemaitre Parameters
        double                          R_inf, R_b; // Hardening: sig_y(R) = sig_Y0 + R_inf (1-exp(-R_b*R))
        double                          a, b;       // Parameters for nonlinear kinematic hardening
        matrix                          C_v;
        matrix                          C_vinv;
    };




    class LemKin_MP : public muesli::sdamageMP
    {
    public:
                                LemKin_MP(const LemKin_Material &m);
        virtual                 ~LemKin_MP(){};
        virtual void            setRandom();

        // tangents
        virtual void            tangentTensor(itensor4& C) const;
        virtual double          shearStiffness() const;

        // energy
        virtual double          deviatoricEnergy() const;
        virtual double          energyDissipationInStep() const;
        virtual double          effectiveStoredEnergy() const;
        virtual double          kineticPotential() const;
        virtual double          storedEnergy() const;
        virtual thPotentials    thermodynamicPotentials() const;
        virtual double          volumetricEnergy() const;

        // stress
        void                    stress(istensor& sigma) const;
        virtual void            deviatoricStress(istensor& s) const;

        // bookkeeping
        virtual void            commitCurrentState();
        virtual void            updateCurrentState(const double theTime, const istensor& strain);

    private:
        const LemKin_Material&  theLemKinMaterial;
        double                  R_n, R_c;
        istensor                beta_n, beta_c;
        matrix                  C12;

        virtual void            residual(realvector& resf, realvector& sigf, realvector& betaf, realvector& betanf, realvector& epsetrialf) const;
        virtual void            dresidual(matrix& jacf, realvector& sigf, realvector& betaf, realvector& epsetrialf) const;

        virtual void            voigt_21(const istensor& mat, realvector& vec) const;
        virtual void            voigt_12(istensor& mat, const realvector& vec) const;
        virtual void            voigt_21eps(const istensor& mat, realvector& vec) const;
        virtual void            voigt_12eps(istensor& mat, const realvector& vec) const;
        virtual void            reframe_24(itensor4& t4_f, const matrix& t2_f) const;

        void                    CheckUpdateState() const; // Checks if the equilibrium equations are fulfilled
        double                  yieldfunction() const; // Compute current yield criterion
        double                  yieldstress(const double R_f) const; // Compute yield stress depending on hardening variable
        double                  dyieldstress(const double R_f) const; // Compute derivative of yield stress depending on hardening variable
    };

};

#endif

/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#ifndef WITHEIGEN
#include "muesli/Math/mrealvector.h"
#include "muesli/Utils/utils.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include "cassert"

#include <cstdarg>
#include <valarray>
#include <vector>




realvector::realvector(size_t dim) :
std::valarray<double>(dim)
{
    for (unsigned i=0; i<dim; i++) (*this)[i] = 0.0;
}




realvector::realvector(size_t dim, const double v1, ...) :
std::valarray<double>(dim)
{
    if (dim>0)
    {
        va_list components;
        (*this)[0] = v1;

        va_start(components, v1);
        for (size_t i=1; i<dim; i++)
        {
            (*this)[i] = va_arg(components, double);
        }
        va_end(components);
    }
}




realvector::realvector(size_t dim, const double* a)
:
std::valarray<double>(a, dim)
{

}


void realvector::abs()
{
    for (size_t i=0; i<size(); i++) (*this)[i] = fabs((*this)[i]);
}




size_t realvector::argmax()
{
    double max = (*this)[0];
    size_t argmax = 0;
    for (size_t i=1; i<size(); i++)
        if ((*this)[i] > max)
        {
            max = (*this)[i];
            argmax = i;
        }

    return argmax;
}


size_t realvector::argmin()
{
    double min = (*this)[0];
    size_t argmin = 0;
    for (size_t i=1; i<size(); i++)
        if ((*this)[i] < min)
         {
            min = (*this)[i];
            argmin = i;
         }
    return argmin;
}


void realvector::changeSign()
{
    for (size_t i=0; i<size(); i++) (*this)[i] = -(*this)[i];
}


double* realvector::components()
{
    return &((*this)[0]);
}




const double* realvector::components() const
{
    return &((*this)[0]);
}




double realvector::dot(const realvector& v) const
{
    assert( v.size() == size() );
    double sum = 0.0;
    for (size_t i=0; i<size(); i++) sum += (*this)[i]*v[i];
    return sum;
}


double realvector::max()
{
    double max = (*this)[0];
    for (size_t i=1; i<size(); i++)
        if ((*this)[i] > max)
            max = (*this)[i];

    return max;
}


double realvector::min()
{
    double min = (*this)[0];
    for (size_t i=1; i<size(); i++)
        if ((*this)[i] < min)
            min = (*this)[i];

    return min;
}


double realvector::norm() const
{
    const double n2 = this->dot(*this);
    return sqrt(n2);
}




void realvector::normalize()
{
    double n = this->norm();
    if (n > 0.0) (*this) /= n;
}




realvector& realvector::operator=(const realvector &v)
{
    assert( v.size() == size() );
    std::valarray<double>::operator=(v);
    return *this;
}




realvector& realvector::operator+=(const realvector &v)
{
    assert( v.size() == size() );
    for (size_t i=0; i<size(); i++) (*this)[i] += v[i];
    return *this;
}




realvector& realvector::operator-=(const realvector &v)
{
    assert( v.size() == size() );
    for (size_t i=0; i<size(); i++) (*this)[i] -= v[i];
    return *this;
}




realvector& realvector::operator*=(const double a)
{
    for (size_t i=0; i<size(); i++) (*this)[i] *= a;
    return *this;
}




realvector operator+(const realvector& left, const realvector& right)
{
    assert( left.size() == right.size() );
    realvector v( left.size() );

    for (size_t i=0; i< left.size(); i++)
        v[i] = left[i] + right[i];

    return v;
}




realvector realvector::operator-() const
{
    realvector n(*this);
    n.changeSign();
    return n;
}




realvector operator-(const realvector &left, const realvector& right)
{
    assert( left.size() == right.size() );
    realvector v( left.size() );

    for (size_t i=0; i< left.size(); i++)
        v[i] = left[i] - right[i];

    return v;
}




realvector operator*(const realvector& v, double a)
{
    realvector w( v.size() );

    for (size_t i=0; i< v.size(); i++)
        w[i] = v[i]*a;

    return w;
}




realvector operator*(double a, const realvector& v)
{
    realvector w( v.size() );

    for (size_t i=0; i< v.size(); i++)
        w[i] = v[i]*a;

    return w;
}




realvector operator/(const realvector& v, double a)
{
    return v*(1./a);
}




bool realvector::operator<(const realvector& other) const
{
    assert( other.size() == this->size() );

    for (unsigned i=0; i< this->size(); i++)
    {
        if ((*this)[i] < other[i])
            return false;
        else if ((*this)[i] > other[i])
            return true;
    }
    return false;
}




std::ostream& operator<<(std::ostream &os, const realvector& v)
{
    if (v.size() <= 3)
    {
        os << "  ";
        for (unsigned a=0; a<v.size(); a++) os << v[a] << " ";
    }
    else
    {
        for (unsigned a=0; a<v.size(); a++) os << "\n " << v[a];
    }

    return os;
}





void realvector::print(std::ostream& of) const
{
    of << std::scientific << std::setprecision(6);
    for (size_t i=0; i<size(); i++)
        of << (*this)[i] << "  ";
}




void realvector::setRandom()
{
    for (size_t i=0; i<size(); i++)
        (*this)[i] = muesli::randomUniform(-1.0, 1.0);
}




void realvector::setZero()
{
    for (size_t i=0; i<size(); i++) (*this)[i] = 0.0;
}




double realvector::squaredNorm() const
{
    return this->dot(*this);
}




void Dcopy(const double *from, double *to, const size_t l)
{
    std::copy(from, from+l, to);
}




void Dzero(double *v, const size_t l)
{
    for (size_t k=0; k<l; k++) v[k] = 0.0;
}

#endif

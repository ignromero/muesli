/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#pragma once
#ifndef _muesli__mrealvector_h
#define _muesli__mrealvector_h

#include <valarray>
#include <iostream>



class realvector : public std::valarray<double>
{
public:
                    realvector(){};
                    realvector(size_t dim);
                    realvector(size_t dim, const double v1, ...);
                    realvector(size_t dim, const double* a);
    virtual         ~realvector(){}

    realvector      operator-() const;
    realvector&     operator=(const realvector &v);
    realvector&     operator+=(const realvector &v);
    realvector&     operator-=(const realvector &v);
    realvector&     operator*=(const double a);
    bool            operator<(const realvector& v) const;

    friend realvector operator+(const realvector& left, const realvector& right);
    friend realvector operator-(const realvector& left, const realvector& right);
    friend realvector operator*(const realvector &v, double a);
    friend realvector operator*(double a, const realvector& v);
    friend realvector operator/(const realvector& v, double a);

    friend std::ostream& operator<<(std::ostream &os, const realvector& v);

    void            abs();
    size_t          argmin();
    size_t          argmax();
    void            changeSign();
    double*         components();
    const double*   components() const;
    double          dot(const realvector& v) const;
    double          max();
    double          min();
    double          norm() const;
    void            normalize();
    void            print(std::ostream& of=std::cout) const;
    void            setRandom();
    void            setZero();
    double          squaredNorm() const;
};




class complexvector
{
public:
                        complexvector(const size_t dimension) : r(dimension), i(dimension){}

    realvector&         real() {return r;}
    const realvector&   real() const {return r;}

    realvector&         imag() {return i;}
    const realvector&   imag() const {return i;}

private:
    realvector          r, i;
};



// utility operations on arrays of doubles
void    Dcopy(const double *from, double *to, const size_t l);
void    Dzero(double *v, const size_t l);


#endif

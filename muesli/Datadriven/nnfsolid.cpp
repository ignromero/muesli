/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#include "nnfsolid.h"
#include "muesli/Utils/utils.h"
#include <math.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <functional>
#include <stdexcept>

using namespace muesli;


std::vector<std::string> nnet::activationNames = {"sigmoid", "relu", "tanh", "linear", "softplus", "selu"};



void nnet::activationFilter(const activationFunction& af, realvector& v) const
{
    auto f_relu    = [](const double a) {return std::max<double>(0.0, a);};
    auto f_sigmoid = [](const double a) {return 1.0/(1.0 + exp(-a));};
    auto f_tanh    = [](const double a) {return tanh(a);};
    auto f_linear  = [](const double a) {return a;};
    auto f_softplus = [](const double a) {return log(exp(a) + 1.0);};
    auto f_selu    = [](const double a) {return (a>0 ? 1.05070098*a : 1.05070098*1.67326324*(exp(a)-1));};

    std::function<double(double)> filter;

    switch (af)
    {
        case ACTIVATION_RELU:    filter = f_relu; break;
        case ACTIVATION_SIGMOID: filter = f_sigmoid; break;
        case ACTIVATION_TANH:    filter = f_tanh; break;
        case ACTIVATION_LINEAR:  filter = f_linear; break;
        case ACTIVATION_SOFTPLUS: filter = f_softplus; break;
        case ACTIVATION_SELU:    filter = f_selu; break;
        default:                 filter = f_linear;
    }

    for (double& a : v) a = filter(a);
}




nnet::nnet(const std::string& filename)
{
    std::ifstream infile(filename.c_str());
    if (!infile.is_open())
    {
        std::cerr << "Error opening file " << filename << std::endl;
    }
    else
    {
        size_t nLayers;
        infile >> nLayers;
        size_t s;
        infile >> s;
        layerSize.push_back(s);

        for (unsigned k=1; k<=nLayers; k++)
        {
            infile >> s;
            layerSize.push_back(s);
            weights.emplace_back(matrix(layerSize[k],layerSize[k-1]));
            biases.emplace_back(realvector(layerSize[k]));
        }

        // Valores para la normalizacion de los inputs y la "de-normalizacion" de los
        // outputs. En el .dat primero se guardan una fila con todos los promedios y
        // luego una fila con todos los desvios estandar (implementado Nestor)
        //unsigned input_output[2];
        input_output[0] = (unsigned) layerSize[0];
        input_output[1] = (unsigned) layerSize[nLayers];

        normMean.emplace_back(realvector(input_output[0]));
        normMean.emplace_back(realvector(input_output[1]));
        normStd.emplace_back(realvector(input_output[0]));
        normStd.emplace_back(realvector(input_output[1]));
        double e;

        // mean values
        for (unsigned i = 0; i < 2; i++)
        {
            for (unsigned j = 0; j < input_output[i]; j++)
            {
                infile >> e;
                normMean[i][j] = e;
            }
        }
        // standard deviation values
        for (unsigned i = 0; i < 2; i++)
        {
            for (unsigned j = 0; j < input_output[i]; j++)
            {
                infile >> e;
                normStd[i][j] = e;
            }
        }

        // the index of the weights and biases is the layer where it takes the input
        // (starting from zero). Hence, the state before the activation filter of
        // layer 1 is:   z1 = A[0]*z0 + B[0]
        double a;
        for (unsigned l=1; l<=nLayers; l++)
        {
            for (unsigned i=0; i<layerSize[l-1]; i++)
            {
                for (unsigned j=0; j<layerSize[l]; j++)
                {
                    infile >> a;
                    weights[l-1](j,i) = a;
                }
            }
        }
        for (unsigned l=1; l<=nLayers; l++){
            for (unsigned j=0; j<layerSize[l]; j++)
            {
                infile >> a;
                biases[l-1][j] = a;
            }
        }

        std::string activationLine, teststring;
        /* Implementado por Nestor, hay que mejorar el tema de que se necesiten dos getline
        (el primero sale vacio), quizas se puede hacer un while con infile >> word; donde
        std::string word;*/
        getline(infile, activationLine);
        getline(infile, activationLine);
        // std::cout << "actLine: " << activationLine << std::endl;
        std::istringstream activationStream(activationLine);
        std::string word;
        while (activationStream >> word)
        {
            muesli::toLowercase(word);
            if (word == "relu")
                activation.push_back(ACTIVATION_RELU);
            else if (word == "linear")
                activation.push_back(ACTIVATION_LINEAR);
            else if (word == "sigmoid")
                activation.push_back(ACTIVATION_SIGMOID);
            else if (word == "tanh")
                activation.push_back(ACTIVATION_TANH);
            else if (word== "softplus")
                activation.push_back(ACTIVATION_SOFTPLUS);
            else if (word== "selu")
                activation.push_back(ACTIVATION_SELU);
            else
                std::cerr << "Error in activation keyword: " << word << ". Keyword ignored" << std::endl;
        }

        if (activation.size() != nLayers)
        {
            std::cerr << "Error!! Incorrect number of activation keywords" << std::endl;
        }
    }
}




nnet::~nnet()
{

}




realvector nnet::operator()(const realvector& input) const
{
    // Normalization of the inputs
    realvector inputnorm(input_output[0]);
    for (unsigned j = 0; j < input_output[0]; j++)
    {
        inputnorm[j] = (input[j] - normMean[0][j])/normStd[0][j];
    }

    std::vector<realvector> partialIns, partialOuts;
    partialIns.push_back(inputnorm);
    for (size_t k=0; k<layerSize.size()-2; k++)
    {
        partialOuts.push_back(weights[k]*partialIns[k] + biases[k]);
        activationFilter(activation[k], partialOuts[k]);
        partialIns.push_back(partialOuts[k]);
    }
    realvector out = weights[layerSize.size()-2]*partialIns[layerSize.size()-2] + biases[layerSize.size()-2];
    activationFilter(activation[layerSize.size()-2], out);

    // De-noralization of the outputs
    for (unsigned j = 0; j < input_output[1]; ++j)
    {
        out[j] = normMean[1][j] + out[j]*normStd[1][j];
    }

    return out;
}




void nnet::info(std::ostream &of) const
{
    of  << "\n   Linear neural network"
        << "\n   Number of layers     : " << layerSize.size()
        << "\n   Input layer size     : " << layerSize[0]
        << "\n   Size of inner layers : ";

    for (unsigned k=1; k<layerSize.size()-1; k++)
        of << " " << layerSize[k];

    of  << "\n   Activation functions : ";
    for (unsigned k=1; k<layerSize.size()-1; k++)
        of << " " << activationNames[activation[k]];

    of  << "\n   Size of output layer : " << *(layerSize.end()-1);
}




nnFsolid::nnFsolid(const std::string& name,
                   const std::string& strainStressFilenamex)
:
finiteStrainMaterial(name),
strainStressFilename(strainStressFilenamex),
strainStressNN(strainStressFilename)
{
}



nnFsolid::~nnFsolid()
{

}




finiteStrainMP* nnFsolid::createMaterialPoint() const
{
    nnFsolidMP* mp = new nnFsolidMP(*this);
    return mp;
}




void nnFsolid::print(std::ostream &of) const
{
    of  << "\n   Data-driven, finite strain constitutive law based on neural networks"
        << "\n   Filename with strain/stress nnet description : " << strainStressFilename;

    strainStressNN.info(of);
}




double nnFsolid::characteristicStiffness() const
{
    return 0.0;
}




void nnFsolid::setRandom()
{

}




bool nnFsolid::test(std::ostream &of)
{
     return true;
}




void nnFsolidMP::updateCurrentState(const double theTime, const itensor& F)
{
    finiteStrainMP::updateCurrentState(theTime,F);
}



nnFsolidMP::nnFsolidMP(const nnFsolid& m)
:
finiteStrainMP(m),
mat(&m)
{
}



// Cauchy stress in terms of principal invariants and the partial derivatives of W w/r to the invariants
void nnFsolidMP::CauchyStress(istensor& sigma) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);
    sigma = 1.0/Jc * istensor::FSFt(Fc, S);
}




void nnFsolidMP::commitCurrentState()
{
    finiteStrainMP::commitCurrentState();
}




void nnFsolidMP::contractWithDeviatoricTangent(const ivector &v1, const ivector& v2, itensor &T) const
{
}




void nnFsolidMP::contractWithMixedTangent(istensor& CM) const
{

}




/* Given the fourth order tensor of elasticities c, and two vectors v, w
 * compute the second order tensor T with components
 *   T_ij = c_ipjq v_p w_q
 *
 *  Note that the result is not symmetric.
 */
void nnFsolidMP::contractWithSpatialTangent(const ivector& v1, const ivector& v2, itensor& T) const
{

}



void nnFsolidMP::contractWithConvectedTangent(const ivector& V1, const ivector& V2, itensor& T) const
{

}



void nnFsolidMP::convectedTangent(itensor4& c) const
{
    numericalConvectedTangent(c);
}



void nnFsolidMP::convectedTangentTimesSymmetricTensor(const istensor& T, istensor& CM) const
{

}



double nnFsolidMP::energyDissipationInStep() const
{
    return 0.0;
}



itensor nnFsolidMP::dissipatedEnergyDF() const
{
    itensor z;
    z.setZero();
    return z;
}



double nnFsolidMP::effectiveStoredEnergy() const
{
    return storedEnergy();
}



void nnFsolidMP::firstPiolaKirchhoffStress(itensor& P) const
{

    itensor Fnp = Fc;
    P.setZero();


    const double inc = 1.0e-5;
    double tn1 = tc; //this->tc;
    // "this" is a const , so it does not allow to use members functions on it
    // that are not declared const so it needs to be casted no a non constant pointer
    nnFsolidMP& theMP = const_cast<nnFsolidMP&>(*this);

    for (unsigned i=0; i<3; i++)
    {
        for (unsigned j=0; j<3; j++)
        {
            double original = this->Fc(i,j);

            Fnp(i,j) = original + inc;
            theMP.updateCurrentState(tn1, Fnp);
            double Wp1 = theMP.storedEnergy();
            //double Wp1 = this->effectiveStoredEnergy();

            Fnp(i,j) = original + 2.0*inc;
            theMP.updateCurrentState(tn1, Fnp);
            double Wp2 = theMP.storedEnergy();
            //double Wp2 = this->effectiveStoredEnergy();

            Fnp(i,j) = original - inc;
            theMP.updateCurrentState(tn1, Fnp);
            double Wm1 = theMP.storedEnergy();
            //double Wm1 = this->effectiveStoredEnergy();

            Fnp(i,j) = original - 2.0*inc;
            theMP.updateCurrentState(tn1, Fnp);
            double Wm2 = theMP.storedEnergy();
            //double Wm2 = this->effectiveStoredEnergy();

            // fourth order approximation of the derivative
            P(i,j) = (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);

            Fnp(i,j) = original;
            theMP.updateCurrentState(tn1, Fnp);
        }
    }
}



materialState nnFsolidMP::getConvergedState() const
{
    materialState state = finiteStrainMP::getConvergedState();

    return state;
}




materialState nnFsolidMP::getCurrentState() const
{
    materialState state = finiteStrainMP::getCurrentState();

    return state;
}




double nnFsolidMP::kineticPotential() const
{
    return 0.0;
}




double nnFsolidMP::plasticSlip() const
{
    return 0.0;
}



void nnFsolidMP::secondPiolaKirchhoffStress(istensor &S) const
{
    itensor P;
    firstPiolaKirchhoffStress(P);
    S = Fc.inverse()*P;
}



void nnFsolidMP::resetCurrentState()
{
    finiteStrainMP::resetCurrentState();
}




void nnFsolidMP::setConvergedState(const double time, const itensor& F)
{

}




void nnFsolidMP::setRandom()
{
    finiteStrainMP::setRandom();
}



double nnFsolidMP::storedEnergy() const
{
    istensor Cc = istensor::tensorTransposedTimesTensor(Fc);

    realvector input(3), output(1);
    input[0] = Cc(0,0); input[1] = Cc(1,1); input[2] = Cc(0,1);

    output = mat->strainStressNN(input);

    return output[0];
}




double nnFsolidMP::volumetricStiffness() const
{
    return 0.0;
}


/****** Implementado por Nestor *******/
/**** NN materials with symmetries ****
double nnFsolidMPSymm::storedEnergy() const
{
    itensor Q;
    double wSymm = 0;
    int nSymm = matrixQ.size()
    for (unsigned i=0; i<nSymm; i++)
    {
        Q = matrixQ[i];
        Fsymm = F*Q;
        theMP.updateCurrentState(tn, Fsymm);
        wSymm += theMP.storedEnergy();

        Fnp(i,j) = original;
        theMP.updateCurrentState(tn1, Fnp);

    }
    wSymm = wSymm/nSymm;

    return wSymm
}



void nnFsolidMPSymm::firstPiolaKirchhoffStress(itensor& Psymm) const
{
    Psymm.setZero();
    itensor Q;
    int nSymm = matrixQ.size()
    for (unsigned i=0; i<nSymm; i++)
    {
        Q = matrixQ[i];
        Fsymm = F*Q;
        theMP.updateCurrentState(tn, Fsymm);
        theMP->firstPiolaKirchhoffStress(P);
        PSymm += P*Q.transpose();

        Fnp(i,j) = original;
        theMP.updateCurrentState(tn1, Fnp);

    }
    PSymm = PSymm/nSymm;
}

**************************************/

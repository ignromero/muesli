/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include <stdio.h>
#include "smechmasshyd.h"
#include "muesli/Smallstrain/smallstrainlib.h"

using namespace muesli;


sMechMassHydMaterial::sMechMassHydMaterial(const std::string& name,
                                     const materialProperties& cl)
:
material(name, cl),
theSSMaterial(0),
diffusivity(0.0),
massExpansion(0.0),
mu0l(0.0),
mu0t(0.0),
muref(0.0),
nl(1),
nt(0),
nt0(20.0),
nt1(0.0),
nt2(0.0),
nta(0.0),
R(8.31),
tref(273.0)
{
    if       (cl.find("subtype elastic") != cl.end())      theSSMaterial = new elasticIsotropicMaterial(name, cl);
    else if  (cl.find("subtype plastic") != cl.end())      theSSMaterial = new splasticMaterial(name, cl);
    else if  (cl.find("subtype viscoelastic") != cl.end()) theSSMaterial = new viscoelasticMaterial(name, cl);
    else if  (cl.find("subtype viscoplastic") != cl.end()) theSSMaterial = new viscoplasticMaterial(name, cl);

    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "beta", massExpansion);
    muesli::assignValue(cl, "mu0l", mu0l);
    muesli::assignValue(cl, "mu0t", mu0t);
    muesli::assignValue(cl, "muref", muref);
    muesli::assignValue(cl, "nl", nl);
    muesli::assignValue(cl, "nt0", nt0);
    muesli::assignValue(cl, "nt1", nt1);
    muesli::assignValue(cl, "nt2", nt2);
    muesli::assignValue(cl, "nta", nta);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "tref", tref);
}




sMechMassHydMaterial::~sMechMassHydMaterial()
{
    if (theSSMaterial != 0) delete(theSSMaterial);
}




bool sMechMassHydMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in sMechMassdHydMaterial. Non-positive diffusivity";
    }

    return ret && theSSMaterial->check();
}




sMechMassHydMP* sMechMassHydMaterial::createMaterialPoint() const
{
    return new sMechMassHydMP(*this);
}




double sMechMassHydMaterial::density() const
{
    return theSSMaterial->density();
}




double sMechMassHydMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_DIFFUSIVITY:    ret = diffusivity; break;
        case PR_MASS_EXP:       ret = massExpansion; break;
        case PR_MU0L:           ret = mu0l; break;
        case PR_MU0T:           ret = mu0t; break;
        case PR_MUREF:          ret = muref; break;
        case PR_NL:             ret = nl; break;
        case PR_NT:             ret = nt0; break;
        case PR_Rgas:           ret = R; break;
        case PR_TREF:           ret = tref; break;

        default:
            ret = theSSMaterial->getProperty(p);
    }
    return ret;
}



double sMechMassHydMaterial::kt() const
{
    double wb = mu0l - mu0t;

    return exp(wb/(R*tref));
}




void sMechMassHydMaterial::print(std::ostream &of) const
{
    of  << "\n Small strain sMechMassHyd material"
        << "\n for coupled stress-hydrogen diffusion problems."
        << "\n Variational formulation: " << isVariational()
        << "\n Surrogate mechanical model: ";
    theSSMaterial->print(of);

    of  << "\n Model constants:"
        << "\n Baseline NILS chemical potential:               " << mu0l
        << "\n Baseline trapped chemical potential:            " << mu0t
        << "\n Gas constant:                                   " << R
        << "\n Mass diffusivity:                               " << diffusivity
        << "\n Mass expansion coefficient:                     " << massExpansion
        << "\n mol/L^3 for NILS hydrogen:                      " << nl
        << "\n NT0 (to compute moles/L^3 of trapped hydrogen): " << nt0
        << "\n NT1 (to compute moles/L^3 of trapped hydrogen): " << nt1
        << "\n NT2 (to compute moles/L^3 of trapped hydrogen): " << nt2
        << "\n NTA (to compute NT in moles/L^3 Avogadro * fv): " << nta
        << "\n Reference chemical potential:                   " << muref
        << "\n Reference temperature:                          " << tref;
}




double sMechMassHydMaterial::referenceChemicalPotential() const
{
    return muref;
}




void sMechMassHydMaterial::setRandom()
{
    int mattype = discreteUniform(0, 1);
    std::string name = "surrogate small strain material";
    materialProperties mp;

    if (mattype == 0)
        theSSMaterial = new elasticIsotropicMaterial(name, mp);

    else if (mattype == 1)
        theSSMaterial = new splasticMaterial(name, mp);

    else if (mattype == 2)
        theSSMaterial = new viscoelasticMaterial(name, mp);

    else if (mattype == 3)
        theSSMaterial = new viscoplasticMaterial(name, mp);

    theSSMaterial->setRandom();
    diffusivity   = randomUniform(0.50, 1.5)*1e-8; // m^2/s
    massExpansion = randomUniform(1.0, 10.0)*1e-5; // m^3 / mol
    mu0l          = randomUniform(20.0, 30.0)*1e3; // J/mol
    mu0t          = randomUniform(25.0, 35.0)*(-1e3); // J/mol
    muref         = randomUniform(10.0, 20.0)*(-1e3); // J/mol
    nl            = (unsigned) randomUniform(1.0, 10.0)*1e5; // mol/m^3
    nt0           = randomUniform(15.0, 24.0);
    nt1           = randomUniform(1.0, 5.0);
    nt2           = randomUniform(1.0, 10.0)*(-1.0);
    nta           = 1.66e-24; // mol * m^3/m^3
    R             = randomUniform(5.0, 10.0); // J/(K�mol)
    tref          = randomUniform(250.0, 350.0); // K
}




void sMechMassHydMaterial::setVariational()
{
    material::setVariational();
}




bool sMechMassHydMaterial::test(std::ostream& of)
{
    bool isok = true;
    setRandom();

    sMechMassHydMP* p = this->createMaterialPoint();

    isok = p->testImplementation(of);
    delete p;

    return isok;
}




double sMechMassHydMaterial::waveVelocity() const
{
    return theSSMaterial->waveVelocity();
}



sMechMassHydMP::sMechMassHydMP(const sMechMassHydMaterial &m)
:
mu_n(0.0), mu_c(0.0),
gradMu_n(0.0, 0.0, 0.0),
gradMu_c(0.0, 0.0, 0.0),
theSSMP(0),
theMechMassHydMaterial(m)
{
    theSSMP = m.theSSMaterial->createMaterialPoint();
}




sMechMassHydMP::~sMechMassHydMP()
{
    if (theSSMP != 0) delete (theSSMP);
}




// TOTAL hydrogen chemical capacity: NILS + TRAPPED hydrogen concentration derived wrt mu
double sMechMassHydMP::chemicalCapacity() const
{
    double cl = chemicalCapacityl();
    double ct = chemicalCapacityt();

    return cl + ct;
}




// TOTAL (NILS + TRAPPED) hydrogen chemical capacity derivative wrt eps
istensor sMechMassHydMP::chemicalCapacityDerivativeWrtEps() const
{
    istensor dcl_deps = chemicalCapacitylDerivativeWrtEps();
    istensor dct_deps = chemicalCapacitytDerivativeWrtEps();

    return dcl_deps + dct_deps;
}




// TOTAL (NILS + TRAPPED) hydrogen chemical capacity derivative wrt mu
double sMechMassHydMP::chemicalCapacityDerivativeWrtMu() const
{
    double dcl_dmu = chemicalCapacitylDerivativeWrtMu();
    double dct_dmu = chemicalCapacitytDerivativeWrtMu();

    return dcl_dmu + dct_dmu;
}




// NILS hydrogen chemical capacity: NILS hydrogen concentration derived wrt mu
double sMechMassHydMP::chemicalCapacityl() const
{
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;

    double       chil   = concentrationl();
    double       thetaL = thetal();

    return chil*(1.0-thetaL)/(R*tref);
}




// NILS hydrogen chemical capacity derived wrt eps
istensor sMechMassHydMP::chemicalCapacitylDerivativeWrtEps() const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    double      dcl_dmu = chemicalCapacitylDerivativeWrtMu();

    const istensor   id = istensor::identity();

    return 3.0*kappa*beta*dcl_dmu*id;
}




// NILS hydrogen chemical capacity derived wrt mu
double sMechMassHydMP::chemicalCapacitylDerivativeWrtMu() const
{
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;

    double       cl     = chemicalCapacityl();
    double       thetaL = thetal();

    return cl*(1.0-2.0*thetaL)/(R*tref);
}




// trapped hydrogen chemical capacity: trapped hydrogen concentration derived wrt mu
double sMechMassHydMP::chemicalCapacityt() const
{
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;

    double       chit   = concentrationt();
    double       thetaT = thetat();

    return chit*(1.0-thetaT)/(R*tref);
}




// trapped hydrogen chemical capacity derived wrt eps
istensor sMechMassHydMP::chemicalCapacitytDerivativeWrtEps() const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    double      dct_dmu = chemicalCapacitytDerivativeWrtMu();

    const istensor   id = istensor::identity();

    return 3.0*kappa*beta*dct_dmu*id;
}




// trapped hydrogen chemical capacity derived wrt mu
double sMechMassHydMP::chemicalCapacitytDerivativeWrtMu() const
{
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;

    double       ct     = chemicalCapacityt();
    double       thetaT = thetat();

    return ct*(1.0-2.0*thetaT)/(R*tref);
}




double sMechMassHydMP::chemicalPotential() const
{
    return mu_c;
}




void sMechMassHydMP::commitCurrentState()
{
    mu_n     = mu_c;
    gradMu_n = gradMu_c;

    theSSMP->commitCurrentState();
}




//TOTAL hydrogen concentration: -d^G/d^mu = -grand canonical potential derived wrt mu
double sMechMassHydMP::concentration() const
{
    double chil = concentrationl();
    double chit = concentrationt();

    return  chil + chit;
}




//TOTAL hydrogen concentration derived wrt strain
istensor sMechMassHydMP::concentrationDerivativeWrtEps() const
{
    istensor dchil_deps = concentrationlDerivativeWrtEps();
    istensor dchit_deps = concentrationtDerivativeWrtEps();

    return  dchil_deps + dchit_deps;
}




//NILS hydrogen concentration: -d^GL/d^muL = -grand canonical potential derived wrt muL
double sMechMassHydMP::concentrationl() const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();
    const double mu0l   = theMechMassHydMaterial.mu0l;
    const double nl     = theMechMassHydMaterial.nl;
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;
    istensor     eps    = theSSMP->getCurrentStrain();
    istensor     eps_p  = theSSMP->getConvergedPlasticStrain();

    double       vol    = (eps-eps_p).trace();
    double       EL     = exp( (mu_c-mu0l + 3.0*kappa*beta*vol)/(R*tref));

    return nl * EL/(1.0+EL);
}




//NILS hydrogen concentration derived wrt strain
istensor sMechMassHydMP::concentrationlDerivativeWrtEps() const
{
    const double beta  = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    return  3.0 * kappa * beta * chemicalCapacityl() * istensor::identity();
}




//trapped hydrogen concentration: -d^GT/d^muT = -grand canonical potential derived wrt muT
double sMechMassHydMP::concentrationt() const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();
    const double mu0t  = theMechMassHydMaterial.mu0t;
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;

    istensor     eps    = theSSMP->getCurrentStrain();
    istensor     eps_p  = theSSMP->getConvergedPlasticStrain();
    double       vol    = (eps-eps_p).trace();

    double       ET     = exp( (mu_c-mu0t + 3.0*kappa*beta*vol)/(R*tref));

    return nt() * ET/(1.0+ET);
}




//trapped hydrogen concentration derived wrt strain
istensor sMechMassHydMP::concentrationtDerivativeWrtEps() const
{
    const double beta  = theMechMassHydMaterial.massExpansion;
    const double kappa = theSSMP->volumetricStiffness();
    istensor     eps_p  = theSSMP->getConvergedPlasticStrain();

    double       ct    = chemicalCapacityt();
    istensor     id    = istensor::identity();

    return  3.0 * kappa * beta * ct * id;
}




double sMechMassHydMP::contractWithMobility(const ivector &v1, const ivector &v2) const
{
    return mobility() * v1.dot(v2);
}




void sMechMassHydMP::contractWithTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithTangent(v1, v2, T);

    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    istensor dsigmacoup_deps = - (3.0*kappa*beta) * concentrationDerivativeWrtEps();

    T += dsigmacoup_deps*itensor::dyadic(v1, v2);
}



void sMechMassHydMP::effectiveContractWithTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithTangent(v1, v2, T);

    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    istensor dsigmacoup_deps = - (3.0*kappa*beta) * effectiveConcentrationDerivativeWrtEps();

    T += dsigmacoup_deps*itensor::dyadic(v1, v2);
}




void sMechMassHydMP::contractWithDeviatoricTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithDeviatoricTangent(v1, v2, T);
}




double sMechMassHydMP::convergedConcentration() const
{
    double chiln = convergedConcentrationl();
    double chitn = convergedConcentrationt();

    return  chiln + chitn;
}




double sMechMassHydMP::convergedConcentrationl() const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();
    const double mu0l   = theMechMassHydMaterial.mu0l;
    const double nl     = theMechMassHydMaterial.nl;
    const double R      = theMechMassHydMaterial.R;
    const double tref   = theMechMassHydMaterial.tref;

    istensor     eps_n  = theSSMP->getConvergedState().theStensor[0];
    istensor     eps_p  = theSSMP->getConvergedPlasticStrain();
    double       vol_n  = (eps_n-eps_p).trace();

    double       EL_n   = exp( (mu_n-mu0l + 3.0*kappa*beta*vol_n)/(R*tref));

    return nl * EL_n/(1.0+EL_n);
}




double sMechMassHydMP::convergedConcentrationt() const
{
    const double beta    = theMechMassHydMaterial.massExpansion;
    const double kappa   = theSSMP->volumetricStiffness();
    const double mu0t    = theMechMassHydMaterial.mu0t;
    const double R       = theMechMassHydMaterial.R;
    const double tref    = theMechMassHydMaterial.tref;

    istensor     eps_n   = theSSMP->getConvergedState().theStensor[0];
    istensor     eps_p   = theSSMP->getConvergedPlasticStrain();
    double       vol_n   = (eps_n-eps_p).trace();

    double       ET_n    = exp( (mu_n-mu0t + 3.0*kappa*beta*vol_n)/(R*tref));

    return nt() * ET_n/(1.0+ET_n);
}




double sMechMassHydMP::deviatoricEnergy() const
{
    return theSSMP->deviatoricEnergy();
}




void sMechMassHydMP::deviatoricStress(istensor& s) const
{
    theSSMP->deviatoricStress(s);
}




double sMechMassHydMP::diffusionPotential() const
{
    double m = mobility();
    ivector g; g = -gradMu_c;

    return -0.5*m*g.squaredNorm();
}




double sMechMassHydMP::diffusiveDissipationInStep() const
{
    ivector j; massFlux(j);
    ivector g = -gradMu_c;
    double dissMass = j.dot(g);

    return dissMass;
}




void sMechMassHydMP::dissipationTangent(itensor4& D) const
{
    theSSMP->dissipationTangent(D);
}




// TOTAL effective chemical capacity
double sMechMassHydMP::effectiveChemicalCapacity() const
{
    double cl    = chemicalCapacityl();
    double cteff = effectiveChemicalCapacityt();

    return cl + cteff;
}




// effective trapped chemical capacity
double sMechMassHydMP::effectiveChemicalCapacityt() const
{
    const double K_T     = theMechMassHydMaterial.kt();
    const double nl     = theMechMassHydMaterial.nl;

    double       chil   = concentrationl();
    double       cl     = chemicalCapacityl();

    double  dchit_dchil = (K_T*nt()*nl)/((nl - chil + K_T*chil)*(nl - chil + K_T*chil));

    return dchit_dchil * cl;
}




// effective chemical capacity derived wrt eps
istensor sMechMassHydMP::effectiveChemicalCapacityDerivativeWrtEps() const
{
    const double K_T     = theMechMassHydMaterial.kt();
    const double nl     = theMechMassHydMaterial.nl;

    double       chil   = concentrationl();
    double       cl     = chemicalCapacityl();

    double dchit_dchil  = (K_T*nl*nt())/((nl - chil + K_T*chil)*(nl - chil + K_T*chil));

    istensor aux1 = 2.0*(1.0-K_T)*dchit_dchil*cl/(nl - chil + K_T*chil) * concentrationlDerivativeWrtEps() ;
    istensor aux2 = (1.0 + dchit_dchil) * chemicalCapacitylDerivativeWrtEps();

    return aux1 + aux2;
}




// effective chemical capacity derived wrt mu
double sMechMassHydMP::effectiveChemicalCapacityDerivativeWrtMu() const
{
    const double K_T     = theMechMassHydMaterial.kt();
    const double nl     = theMechMassHydMaterial.nl;

    double       chil   = concentrationl();
    double       cl     = chemicalCapacityl();

    double dchit_dchil  = (K_T*nl*nt())/((nl - chil + K_T*chil)*(nl - chil + K_T*chil));

    double aux1 = 2.0*(1.0-K_T)*dchit_dchil*cl*cl/(nl - chil + K_T*chil);
    double aux2 = (1.0 + dchit_dchil) * chemicalCapacitylDerivativeWrtMu();

    return aux1 + aux2;
}




double sMechMassHydMP::effectiveConcentration() const
{
    double chil    = concentrationl();
    double chiteff = effectiveConcentrationt();

    return chil + chiteff;
}




istensor sMechMassHydMP::effectiveConcentrationDerivativeWrtEps() const
{
    istensor dchil_deps    = concentrationlDerivativeWrtEps();
    istensor dchiteff_deps = effectiveConcentrationtDerivativeWrtEps();

    return dchil_deps + dchiteff_deps;
}




double sMechMassHydMP::effectiveConcentrationt() const
{
    const double K_T     = theMechMassHydMaterial.kt();
    const double nl     = theMechMassHydMaterial.nl;

    double       chil   = concentrationl();

    return K_T*nt()*chil /(nl-chil+K_T*chil);
}




istensor sMechMassHydMP::effectiveConcentrationtDerivativeWrtEps() const
{
    const double K_T     = theMechMassHydMaterial.kt();
    const double nl     = theMechMassHydMaterial.nl;

    double       thetaL = thetal();

    istensor dchil_deps  = concentrationlDerivativeWrtEps();

    return (K_T*nt()) /(nl * (1.0 - thetaL + K_T*thetaL)*(1.0 - thetaL + K_T*thetaL)) * dchil_deps;
}




double sMechMassHydMP::effectiveConvergedConcentration() const
{
    double chiln    = convergedConcentrationl();
    double chiteffn = convergedConcentrationt();

    return chiln + chiteffn;
}




double sMechMassHydMP::effectiveConvergedConcentrationt() const
{
    const double K_T       = theMechMassHydMaterial.kt();
    const double nl       = theMechMassHydMaterial.nl;

    double       thetal_n = convergedConcentrationl()/nl;
    istensor     eps_p    = theSSMP->getConvergedPlasticStrain();

    return K_T*nt()*thetal_n /(1.0 - thetal_n + K_T*thetal_n);
}




double sMechMassHydMP::effectiveGrandCanonicalPotential() const
{
    return freeEnergy() - mu_c*effectiveConcentration();
}




double sMechMassHydMP::effectiveStoredEnergy() const
{
    double dt_Psistar = energyDissipationInStep();
    double phi        = freeEnergy();

    return phi + dt_Psistar;
}




// d^Geff/d^eps G derived wrt strain
void sMechMassHydMP::effectiveStress(istensor& sigma) const
{
    const double K_T      = theMechMassHydMaterial.kt();
    const double beta    = theMechMassHydMaterial.massExpansion;
    const double kappa   = theSSMP->volumetricStiffness();
    const double mu0l    = theMechMassHydMaterial.mu0l;
    const double muref   = theMechMassHydMaterial.muref;
    const double nl      = theMechMassHydMaterial.nl;
    const double R       = theMechMassHydMaterial.R;
    const double tref    = theMechMassHydMaterial.tref;

    double       EL0     = exp( (muref-mu0l)/(R*tref));
    double       thetal0 = EL0/(1.0+EL0);
    double       chil0   = nl*thetal0;

    double       chit0   = K_T*nt()*thetal0/ (1.0-thetal0+K_T*thetal0);

    theSSMP->stress(sigma);
    sigma -= 3.0*kappa*beta*(concentrationl()-chil0)*istensor::identity() + 3.0*kappa*beta*(effectiveConcentrationt()-chit0)*istensor::identity();
}




// d^Geff/(deps dmu) effective stress derived wrt chemical potential
void sMechMassHydMP::effectiveStressChemicalTensor(istensor& M) const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    M = - 3.0*kappa*beta*effectiveChemicalCapacity()*istensor::identity();
}




void sMechMassHydMP::effectiveStressTangentTensor(itensor4& c) const
{
    const double beta  = theMechMassHydMaterial.massExpansion;
    const double kappa = theSSMP->volumetricStiffness();

    istensor aux       = - (3.0*kappa*beta)*effectiveConcentrationDerivativeWrtEps();

    istensor id2;
    id2 = istensor::identity();

    itensor4 c_diff;
    c_diff = itensor4::dyadic(id2, aux);

    theSSMP->tangentTensor(c);

    c += c_diff;
}




double sMechMassHydMP::energyDissipationInStep() const
{
    double dissMech = theSSMP->energyDissipationInStep();

    ivector j; massFlux(j);
    ivector g = -gradMu_c;
    double dissMass = j.dot(g);

    return dissMech + dissMass;
}




double sMechMassHydMP::effectiveFreeEnergy() const
{
    const double  beta    = theMechMassHydMaterial.massExpansion;
    const double  kappa   = theSSMP->volumetricStiffness();
    const double  K_T      = theMechMassHydMaterial.kt();
    const double  mu0l    = theMechMassHydMaterial.mu0l;
    const double  mu0t    = theMechMassHydMaterial.mu0t;
    const double  muref   = theMechMassHydMaterial.muref;
    const double  nl      = theMechMassHydMaterial.nl;
    const double  R       = theMechMassHydMaterial.R;
    const double  tref    = theMechMassHydMaterial.tref;

    double        chil    = concentrationl();
    double        chit    = effectiveConcentrationt();
    double        thetaL  = thetal();
    double        thetaT  = effectiveThetat();
    istensor      eps_c   = theSSMP->getCurrentStrain();
    istensor      eps_p   = theSSMP->getConvergedPlasticStrain();

    double        vol     = (eps_c-eps_p).trace();

    const double  EL0     = exp( (muref-mu0l)/(R*tref));
    const double  thetal0 = EL0/(1.0+EL0);
    const double  chil0   = nl*thetal0;
    const double  chit0   = K_T*nt()*thetal0/ (1.0-thetal0+K_T*thetal0);

    double fmec   = theSSMP->storedEnergy();

    double fcoup  = - 3.0*kappa*beta*(chil-chil0)*vol - 3.0*kappa*beta*(chit-chit0)*vol;

    double fmass = mu0l*chil + R*tref*nl*(thetaL*log(thetaL)+(1.0-thetaL)*log(1.0-thetaL)) + mu0t*chit + R*tref*nt()*(thetaT*log(thetaT)+(1.0-thetaT)*log(1.0-thetaT));

    return fmec + fcoup + fmass;
}




double sMechMassHydMP::freeEnergy() const
{
    const double  beta    = theMechMassHydMaterial.massExpansion;
    const double  kappa   = theSSMP->volumetricStiffness();
    const double  K_T      = theMechMassHydMaterial.kt();
    const double  mu0l    = theMechMassHydMaterial.mu0l;
    const double  mu0t    = theMechMassHydMaterial.mu0t;
    const double  muref   = theMechMassHydMaterial.muref;
    const double  nl      = theMechMassHydMaterial.nl;
    const double  R       = theMechMassHydMaterial.R;
    const double  tref    = theMechMassHydMaterial.tref;

    double        chil    = concentrationl();
    double        chit    = concentrationt();
    double        thetaL  = thetal();
    double        thetaT  = thetat();
    istensor      eps_c   = theSSMP->getCurrentStrain();
    istensor      eps_p   = theSSMP->getConvergedPlasticStrain();

    double        vol     = (eps_c-eps_p).trace();

    const double  EL0     = exp( (muref-mu0l)/(R*tref));
    const double  thetal0 = EL0/(1.0+EL0);
    const double  chil0   = nl*thetal0;
    const double  ET0     = exp( (muref-mu0t)/(R*tref));
    const double  thetat0 = ET0/(1.0+ET0);
    const double  chit0   = nt()*thetat0;

    double fmec   = theSSMP->storedEnergy();

    double fcoup  = - 3.0*kappa*beta*(chil-chil0)*vol - 3.0*kappa*beta*(chit-chit0)*vol;

    double fmass = mu0l*chil + R*tref*nl*(thetaL*log(thetaL)+(1.0-thetaL)*log(1.0-thetaL)) + mu0t*chit + R*tref*nt()*(thetaT*log(thetaT)+(1.0-thetaT)*log(1.0-thetaT));

    return fmec + fcoup + fmass;
}




bool sMechMassHydMP::freeEnergyCheck() const
{
    bool ret = true;

    double       thetaL = thetal();
    double       thetaT = thetat();

    if (thetaL == 1.0)
    {
        ret = false;
        std::cout << "Error in sMechMassHydMP: thetaL = 1.";
    }

    else if (thetaL == 0.0)
    {
        ret = false;
        std::cout << "Error in sMechMassHydMP: thetaL = 0.";
    }

    else if (thetaT == 1.0)
    {
        ret = false;
        std::cout << "Error in sMechMassHydMP: thetaT = 1.";
    }

    else if (thetaT == 0.0)
    {
        ret = false;
        std::cout << "Error in sMechMassHydMP: thetaT = 0.";
    }

    return ret;
}




istensor sMechMassHydMP::getConvergedPlasticStrain() const
{
    return theSSMP->getConvergedPlasticStrain();
}




materialState sMechMassHydMP::getConvergedState() const
{
    materialState mat;

    mat = theSSMP->getConvergedState();
    mat.theDouble.push_back(mu_n);
    mat.theVector.push_back(gradMu_n);

    return mat;
}




istensor sMechMassHydMP::getCurrentPlasticStrain() const
{
    return theSSMP->getCurrentPlasticStrain();
}




materialState sMechMassHydMP::getCurrentState() const
{
    // first store the state of the ssmp
    materialState mat;
    mat = theSSMP->getCurrentState();

    // then append the hydrogen diffusion data
    mat.theDouble.push_back(mu_c);
    mat.theVector.push_back(gradMu_c);

    return mat;
}




istensor& sMechMassHydMP::getCurrentStrain()
{
    return theSSMP->getCurrentState().theStensor[0];
}




double sMechMassHydMP::grandCanonicalPotential() const
{
    return freeEnergy() - mu_c*concentration();
}




double sMechMassHydMP::internalEnergy() const
{
    double U = freeEnergy();

    return U;
}




double sMechMassHydMP::kineticPotential() const
{
    double Omegamec = theSSMP->kineticPotential();
    double Omegahyd = diffusionPotential();

    return Omegamec + Omegahyd;
}




void sMechMassHydMP::massFlux(ivector &q) const
{
    double m = mobility();

    q = -m * gradMu_c;
}




void sMechMassHydMP::massFluxDerivativeWrtEps(ivector &qprime) const
{
    istensor mprime = mobilityDerivativeWrtEps();

    qprime = -mprime*gradMu_c;
}




void sMechMassHydMP::massFluxDerivativeWrtMu(ivector &qprime) const
{
    double mprime = mobilityDerivativeWrtMu();

    qprime = -mprime*gradMu_c;
}




double sMechMassHydMP::mechanicalDissipationInStep() const
{
    return theSSMP->energyDissipationInStep();
}




itensor sMechMassHydMP::mechanicalDissipationInStepDEps() const
{
    itensor mechDiss; mechDiss.setZero();

    return mechDiss;
}




double sMechMassHydMP::mechanicalDissipationInStepDMu() const
{
    return 0.0;
}




double sMechMassHydMP::mobility() const
{
    const double beta        = theMechMassHydMaterial.massExpansion;
    const double kappa       = theSSMP->volumetricStiffness();
    const double diffusivity = theMechMassHydMaterial.diffusivity;
    const double mu0l        = theMechMassHydMaterial.mu0l;
    const double nl          = theMechMassHydMaterial.nl;
    const double R           = theMechMassHydMaterial.R;
    const double tref        = theMechMassHydMaterial.tref;

    istensor     eps_n       = theSSMP->getConvergedState().theStensor[0];
    istensor     eps_p       = theSSMP->getConvergedPlasticStrain();
    double       vol_n       = (eps_n-eps_p).trace();

    double       EL          = exp( (mu_c-mu0l + 3.0*kappa*beta*vol_n)/(R*tref));
    double       chil_n      = nl * EL/(1.0+EL);

    return diffusivity*chil_n/(R*tref);
}




istensor sMechMassHydMP::mobilityDerivativeWrtEps() const
{
    istensor dm_deps;

    dm_deps.setZero();

    return dm_deps;
}




double sMechMassHydMP::mobilityDerivativeWrtMu() const
{
    const double beta        = theMechMassHydMaterial.massExpansion;
    const double diffusivity = theMechMassHydMaterial.diffusivity;
    const double kappa       = theSSMP->volumetricStiffness();
    const double mu0l        = theMechMassHydMaterial.mu0l;
    const double nl          = theMechMassHydMaterial.nl;
    const double R           = theMechMassHydMaterial.R;
    const double tref        = theMechMassHydMaterial.tref;

    istensor     eps_n       = theSSMP->getConvergedState().theStensor[0];
    istensor     eps_p       = theSSMP->getConvergedPlasticStrain();
    double       vol_n       = (eps_n-eps_p).trace();

    double       EL          = exp( (mu_c-mu0l + 3.0*kappa*beta*vol_n)/(R*tref));
    double       ELpmu       = EL/(R*tref);

    double       dchil_dmu   = nl*ELpmu/((1.0+EL)*(1.0+EL));

    return diffusivity*dchil_dmu/(R*tref);
}




double sMechMassHydMP::nt() const
{
    const double nt0     = theMechMassHydMaterial.nt0;
    const double nt1     = theMechMassHydMaterial.nt1;
    const double nt2     = theMechMassHydMaterial.nt2;
    const double nta     = theMechMassHydMaterial.nta;

    istensor     eps_p   = theSSMP->getConvergedPlasticStrain();

    double     pslip_eq  = sqrt(2.0/3.0)*eps_p.norm();

    return     nta*exp(nt0 - nt1 *exp(nt2*pslip_eq));
}




double sMechMassHydMP::plasticSlip() const
{
    return theSSMP->plasticSlip();
}




double sMechMassHydMP::pressure() const
{
    return theSSMP->pressure();
}




void sMechMassHydMP::resetCurrentState()
{
    mu_c     = mu_n;
    gradMu_c = gradMu_n;
    theSSMP->resetCurrentState();
}




void sMechMassHydMP::setRandom()
{
    mu_c = randomUniform(0.7, 1.3) * theMechMassHydMaterial.mu0l;
    gradMu_c.setRandom();
    theSSMP->setRandom();
}




// d^G/d^eps G derived wrt strain
void sMechMassHydMP::stress(istensor& sigma) const
{
    const double beta    = theMechMassHydMaterial.massExpansion;
    const double kappa   = theSSMP->volumetricStiffness();
    const double mu0l    = theMechMassHydMaterial.mu0l;
    const double mu0t    = theMechMassHydMaterial.mu0t;
    const double muref   = theMechMassHydMaterial.muref;
    const double nl      = theMechMassHydMaterial.nl;
    const double R       = theMechMassHydMaterial.R;
    const double tref    = theMechMassHydMaterial.tref;

    double       EL0     = exp( (muref-mu0l)/(R*tref));
    double       thetal0 = EL0/(1.0+EL0);
    double       chil0   = nl*thetal0;

    double       ET0     = exp( (muref-mu0t)/(R*tref));
    double       thetat0 = ET0/(1.0+ET0);

    double       chit0   = nt()*thetat0;

    theSSMP->stress(sigma);

    sigma -= 3.0*kappa*beta*(concentrationl()-chil0)* istensor::identity() + 3.0*kappa*beta*(concentrationt()-chit0)*istensor::identity();
}




// d^G/(deps dmu) stress derived wrt chemical potential
void sMechMassHydMP::stressChemicalTensor(istensor& M) const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    double c            = chemicalCapacity();

    M = -3.0 * kappa * beta * c * istensor::identity();
}




// d^G/(dE dE)
void sMechMassHydMP::stressTangentTensor(itensor4& c) const
{
    const double beta   = theMechMassHydMaterial.massExpansion;
    const double kappa  = theSSMP->volumetricStiffness();

    istensor aux        = - (3.0*kappa*beta)*concentrationDerivativeWrtEps();

    istensor id2;
    id2 = istensor::identity();

    itensor4 c_diff;
    c_diff = itensor4::dyadic(id2, aux);

    theSSMP->tangentTensor(c);

    c += c_diff;
}




bool sMechMassHydMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    // set a random update in the material
    istensor eps;
    eps.setZero();

    double mu;
    mu = randomUniform(0.8, 1.2) * theMechMassHydMaterial.mu0l;

    ivector gradMu;
    gradMu.setRandom();

    sMechMassHydMP* ssmp = const_cast<sMechMassHydMP*>(this);

    ssmp->updateCurrentState(0.0, eps, mu, gradMu);
    ssmp->commitCurrentState();

    double tn1 = muesli::randomUniform(0.1,1.0);
    eps.setRandom();
    sMechMassHydMP& theMP = const_cast<sMechMassHydMP&>(*this);
    theMP.updateCurrentState(tn1, eps, mu, gradMu);


    //Derivatives with respect to mu:
    //1. concentration; 2. effectiveConcentration 3. chemicalCapacity; 4. effectiveChemicalCapacity; 5. chemicalCapacityDerivativeWrtMu; 6. effectiveChemicalCapacityDerivativeWrtMu; 7. stressChemicalTensor; 8. effectiveStressChemicalTensor; 9. massFluxDerivativeWrtMu; 10. mobilityDerivativeWrtMu;
    //1. Concentration test
    if (true)
    {
        // Compute numerical value of concentration chi =  -(d GCP / d mu)
        const double inc = 1.0e-4;

        double num_chi;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double GCP_p1          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double GCP_p2          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double GCP_m1          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double GCP_m2          = grandCanonicalPotential();

        // fourth order approximation of the derivative
        num_chi = - (-GCP_p2 + 8.0*GCP_p1 - 8.0*GCP_m1 + GCP_m2)/(12.0*inc);

        // programmed concentration
        double pr_chi = concentrationl() + concentrationt();

        // compare concentration with derivative of GCP wrt mu
        // relative error less than 0.01%
        double error = num_chi - pr_chi;

        isok = (fabs(error)/fabs(pr_chi) < 1e-4);
        os << "\n   1. Comparing concentration with - [d GCP / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in concentration computation %e " << fabs(error)/fabs(pr_chi);
            os << "\n Programmed concentration: " << pr_chi;
            os << "\n Numeric concentration:    " << num_chi;
            os << "\n concentrationl():" << concentrationl();
            os << "\n concentrationt():" << concentrationt();
        }

    }

    //2. Effective concentration test
    if (true)
    {
        // Compute numerical value of effective concentration chi =  -(d GCPeff / d mu)
        const double inc = 1.0e-4;

        double num_chi;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double GCP_p1          = effectiveGrandCanonicalPotential();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double GCP_p2          = effectiveGrandCanonicalPotential();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double GCP_m1          = effectiveGrandCanonicalPotential();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double GCP_m2          = effectiveGrandCanonicalPotential();

        // fourth order approximation of the derivative
        num_chi = - (-GCP_p2 + 8.0*GCP_p1 - 8.0*GCP_m1 + GCP_m2)/(12.0*inc);

        // programmed effective concentration
        double pr_chi = effectiveConcentration();

        // compare effective concentration with derivative of GCPeff wrt mu
        // relative error less than 0.01%
        double error = num_chi - pr_chi;

        isok = (fabs(error)/fabs(pr_chi) < 1e-4);
        os << "\n   2. Comparing effective concentration with - [d GCPeff / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effective concentration computation %e " << fabs(error)/fabs(pr_chi);
            os << "\n Programmed effective concentration: " << pr_chi;
            os << "\n Numeric effective concentration:    " << num_chi;
        }

    }

    //3. TOTAL chemicalCapacity test
    if (true)
    {
        // Compute numerical value of chemical capacity chemCap = d chi / d mu
        const double inc = 1.0e-3;

        double num_chemCap;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double chi_p1          = concentrationl() + concentrationt();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double chi_p2          = concentrationl() + concentrationt();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double chi_m1          = concentrationl() + concentrationt();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double chi_m2          = concentrationl() + concentrationt();

        // fourth order approximation of the derivative
        num_chemCap = (-chi_p2 + 8.0*chi_p1 - 8.0*chi_m1 + chi_m2)/(12.0*inc);

        // programmed chemicalCapacity
        double pr_chemCap = chemicalCapacityl() + chemicalCapacityt();


        // compare chemical capacity with derivative of concentration wrt mu
        // relative error less than 0.01%
        double error = num_chemCap - pr_chemCap;

        isok = (fabs(error)/fabs(pr_chemCap) < 1e-4);
        os << "\n   3. Comparing chemicalCapacity with  [d chi / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
            os << "\n Programmed chemical capacity: " << pr_chemCap;
            os << "\n Numeric chemical capacity:    " << num_chemCap;
        }
    }

    //4. TOTAL effectiveChemicalCapacity test
    if (true)
    {
        // Compute numerical value of effective chemical capacity effChemCap = d chieff / d mu
        const double inc = 1.0e-3;

        double num_effChemCap;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double effChi_p1          = effectiveConcentration();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double effChi_p2          = effectiveConcentration();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double effChi_m1          = effectiveConcentration();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double effChi_m2          = effectiveConcentration();

        // fourth order approximation of the derivative
        num_effChemCap = (-effChi_p2 + 8.0*effChi_p1 - 8.0*effChi_m1 + effChi_m2)/(12.0*inc);

        // programmed effectiveChemicalCapacity
        double pr_effChemCap = effectiveChemicalCapacity();


        // compare effective chemical capacity with derivative of effective concentration wrt mu
        // relative error less than 0.01%
        double error = num_effChemCap - pr_effChemCap;

        isok = (fabs(error)/fabs(pr_effChemCap) < 1e-4);
        os << "\n   4. Comparing effectiveChemicalCapacity with  [d chieff / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effectiveChemicalCapacity computation %e " << fabs(error)/fabs(pr_effChemCap);
            os << "\n Programmed chemical capacity: " << pr_effChemCap;
            os << "\n Numeric chemical capacity:    " << num_effChemCap;
        }
    }

    //5. chemicalCapacityDerivativeWrtMu test
    if (true)
    {
        // Compute numerical value of chemical capacity derivative chemCapDerWrtMu = d chemCapl / d mu
        const double inc = 1.0e-3;

        double num_chemCapDer;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double chemCap_p1    = chemicalCapacity();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double chemCap_p2    = chemicalCapacity();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double chemCap_m1    = chemicalCapacity();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double chemCap_m2    = chemicalCapacity();

        // fourth order approximation of the derivative
        num_chemCapDer = (-chemCap_p2 + 8.0*chemCap_p1 - 8.0*chemCap_m1 + chemCap_m2)/(12.0*inc);

        // programmed chemicalCapacityDerivativeWrtMu
        double pr_chemCapDer = chemicalCapacityDerivativeWrtMu();


        // Compare chemical capacity derivative with derivative of chemical capacity wrt mu
        // relative error less than 0.5%
        double error = num_chemCapDer - pr_chemCapDer;

        isok = (fabs(error)/fabs(pr_chemCapDer) < 1e-2);
        os << "\n   5. Comparing chemicalCapacityDerivativeWrtMu with [ d chemCap / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in chemicalCapacityDerivativeWrtMu computation %e " << fabs(error)/fabs(pr_chemCapDer);
            os << "\n Programmed chemical capacity derivative wrt mu: " << pr_chemCapDer;
            os << "\n Numeric chemical capacity derivative wrt mu:    " << num_chemCapDer;
        }
    }

    //6. effectiveChemicalCapacityDerivativeWrtMu test
    if (true)
    {
        // Compute numerical value of effective chemical capacity derivative effChemCapDerWrtMu = d effChemCap / d mu
        const double inc = 1.0e-4;

        double num_effChemCapDer;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double effChemCap_p1    = effectiveChemicalCapacity();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double effChemCap_p2    = effectiveChemicalCapacity();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double effChemCap_m1    = effectiveChemicalCapacity();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double effChemCap_m2    = effectiveChemicalCapacity();

        // fourth order approximation of the derivative
        num_effChemCapDer = (-effChemCap_p2 + 8.0*effChemCap_p1 - 8.0*effChemCap_m1 + effChemCap_m2)/(12.0*inc);

        // programmed effectiveChemicalCapacitytDerivativeWrtMu
        double pr_effChemCapDer = effectiveChemicalCapacityDerivativeWrtMu();

        // Compare effective chemical capacity derivative wrt mu with derivative of effective chemical capacity wrt mu
        // relative error less than 0.01%
        double error = num_effChemCapDer - pr_effChemCapDer;

        isok = (fabs(error)/fabs(pr_effChemCapDer) < 1e-4);
        os << "\n   6. Comparing effectiveChemicalCapacityDerivativeWrtMu with [ d effChemCap / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effChemicalCapacityDerivativeWrtMu computation %e " << fabs(error)/fabs(pr_effChemCapDer);
            os << "\n Programmed effective chemical capacity derivative wrt mu: " << pr_effChemCapDer;
            os << "\n Numeric effective chemical capacity derivative wrt mu:    " << num_effChemCapDer;
        }
    }

    //7. stressChemicalTensor test
    if (true)
    {
        // Compute numerical value of stress chemical tensor stressChemicalTensor = d stress / d mu
        const double inc = 1.0e-4;

        istensor num_stressChemTensor;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        istensor stress_p1; stress(stress_p1);

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        istensor stress_p2; stress(stress_p2);

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        istensor stress_m1; stress(stress_m1);

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        istensor stress_m2; stress(stress_m2);

        // fourth order approximation of the derivative
        num_stressChemTensor = (-stress_p2 + 8.0*stress_p1 - 8.0*stress_m1 + stress_m2)/(12.0*inc);

        // programmed stress chemical tensor
        istensor pr_stressChemTensor; stressChemicalTensor(pr_stressChemTensor);

        // Compare stress chemical tensor with derivative of stress derivative wrt mu
        // relative error less than 0.01%
        istensor error = num_stressChemTensor - pr_stressChemTensor;

        isok = (error.norm()/pr_stressChemTensor.norm() < 1e-4);
        os << "\n   7. Comparing stressChemicalTensor with [ d stress / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in stressChemicalTensor computation %e " << error.norm()/pr_stressChemTensor.norm();
            os << "\n Programmed stress derivative wrt mu: " << pr_stressChemTensor;
            os << "\n Numeric stress derivative wrt mu:    " << num_stressChemTensor;
        }
    }

    //8. effectiveStressChemicalTensor test
    if (true)
    {
        // Compute numerical value of effective stress chemical tensor effectiveStressChemicalTensor = d stresseff / d mu
        const double inc = 1.0e-4;

        istensor num_effStressChemTensor;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        istensor effStress_p1; effectiveStress(effStress_p1);

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        istensor effStress_p2; effectiveStress(effStress_p2);

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        istensor effStress_m1; effectiveStress(effStress_m1);

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        istensor effStress_m2; effectiveStress(effStress_m2);

        // fourth order approximation of the derivative
        num_effStressChemTensor = (-effStress_p2 + 8.0*effStress_p1 - 8.0*effStress_m1 + effStress_m2)/(12.0*inc);

        // programmed effecive stress chemical tensor
        istensor pr_effStressChemTensor; effectiveStressChemicalTensor(pr_effStressChemTensor);

        // Compare stress chemical tensor with derivative of stress derivative wrt mu
        // relative error less than 0.01%
        istensor error = num_effStressChemTensor - pr_effStressChemTensor;

        isok = (error.norm()/pr_effStressChemTensor.norm() < 1e-4);
        os << "\n   8. Comparing effectiveStressChemicalTensor with [ d stress eff / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effectiveStressChemicalTensor computation %e " << error.norm()/pr_effStressChemTensor.norm();
            os << "\n Programmed effective stress derivative wrt mu: " << pr_effStressChemTensor;
            os << "\n Numeric effective stress derivative wrt mu:    " << num_effStressChemTensor;
        }
    }

    // 9. massFluxDerivativeWrtMu test
    if (true)
    {
        // Compute numerical value of mass flux derivative wrt mu massFluxDerivativeWrtMu = d massFlux / d mu
        const double inc = 1.0e-3;

        ivector num_jDerWrtMu;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        ivector jDer_p1; massFlux(jDer_p1);

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        ivector jDer_p2; massFlux(jDer_p2);

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        ivector jDer_m1; massFlux(jDer_m1);

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        ivector jDer_m2; massFlux(jDer_m2);

        // fourth order approximation of the derivative
        num_jDerWrtMu = (-jDer_p2 + 8.0*jDer_p1 - 8.0*jDer_m1 + jDer_m2)/(12.0*inc);

        // programmed massFluxDerivativeWrtMu
        ivector pr_jDerWrtMu;
        massFluxDerivativeWrtMu(pr_jDerWrtMu);

        // compare mobilityDerivativeWrtMu with derivative of mobility wrt mu
        // relative error less than 0.01%
        ivector error = num_jDerWrtMu - pr_jDerWrtMu;

        isok = (error.norm()/pr_jDerWrtMu.norm() < 1e-4);
        os << "\n   9. Comparing massFluxDerivativeWrtMu with [d massFlux / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in massFluxDerivativeWrtMu computation %e " << error.norm()/pr_jDerWrtMu.norm();
            os << "\n Programmed mass flux derivative wrt mu: " << pr_jDerWrtMu;
            os << "\n Numeric mass flux derivative wrt mu:    " << num_jDerWrtMu;
        }
    }

    // 10. mobilityDerivativeWrtMu test
    if (true)
    {
        // Compute numerical value of mobility derivative mobilityDerivativeWrtMu = -(d mobility / d mu)
        const double inc = 1.0e-3;

        double num_mDerWrtMu;

        theMP.updateCurrentState(tn1, eps, mu+inc, gradMu);
        double mDer_p1    = mobility();

        theMP.updateCurrentState(tn1, eps, mu+2.0*inc, gradMu);
        double mDer_p2    = mobility();

        theMP.updateCurrentState(tn1, eps, mu-inc, gradMu);
        double mDer_m1    = mobility();

        theMP.updateCurrentState(tn1, eps, mu-2.0*inc, gradMu);
        double mDer_m2    = mobility();

        // fourth order approximation of the derivative
        num_mDerWrtMu = (-mDer_p2 + 8.0*mDer_p1 - 8.0*mDer_m1 + mDer_m2)/(12.0*inc);

        // programmed mobilityDerivativeWrtMu
        double pr_mDerWrtMu = mobilityDerivativeWrtMu();

        // compare mobilityDerivativeWrtMu with derivative of mobility wrt mu
        // relative error less than 0.01%
        double error = num_mDerWrtMu - pr_mDerWrtMu;

        isok = (fabs(error)/fabs(pr_mDerWrtMu) < 1e-4);
        os << "\n   10. Comparing mobilityDerivativeWrtMu with [d m / d mu ].";

        if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in mobilityDerivativeWrtMu computation %e " << fabs(error)/fabs(pr_mDerWrtMu);
            os << "\n Programmed mobility derivative wrt mu: " << pr_mDerWrtMu;
            os << "\n Numeric mobility derivative wrt mu:    " << num_mDerWrtMu;
        }
    }

    //Derivatives with respect to eps:
    //11. stress; 12. effectiveStress; 13. concentrationDerivativeWrtEps; 14. effectiveConcentrationDerivativeWrtEps; 15. chemicalCapacityDerivativeWrtEps ; 16. effectiveChemicalCapacityDerivativeWrtEps; 17. massFluxDerivativeWrtEps; 18. mobilityDerivativeWrtEps; 19. stressTangentTensor; 20. effectiveStressTangentTensor;

    //11. stress test
    if (true)
    {
        // numerical differentiation stress
        istensor num_stress;
        num_stress.setZero();
        const double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double psi_p1 = grandCanonicalPotential();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double psi_p2 = grandCanonicalPotential();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double psi_m1 = grandCanonicalPotential();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double psi_m2 = grandCanonicalPotential();

                // fourth order approximation of the derivative
                double der = (-psi_p2 + 8.0*psi_p1 - 8.0*psi_m1 + psi_m2)/(12.0*inc);
                num_stress(i,j) = der;
                if (i != j) num_stress(i,j) *= 0.5;

                num_stress(j,i) = num_stress(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }
        // programmed stress
        istensor pr_stress;
        stress(pr_stress);

        // Compare stress with derivative of free energy wrt mu
        // relative error less than 0.01%
        istensor error = num_stress - pr_stress;

        isok = (error.norm()/pr_stress.norm() < 1.0e-3);

        os << "\n   11. Comparing stress with [ d free energy / d eps].";
        if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in stress computation:       " <<  error.norm()/pr_stress.norm();
            os << "\n      Programmed stress:                          " << pr_stress;
            os << "\n      Numeric derivative of free energy wrt eps:  " << num_stress;
        }
    }

    //12. effective stress test
    if (true)
    {
        // numerical differentiation effective stress
        istensor num_effStress;
        num_effStress.setZero();
        const double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effPsi_p1 = effectiveGrandCanonicalPotential();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effPsi_p2 = effectiveGrandCanonicalPotential();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effPsi_m1 = effectiveGrandCanonicalPotential();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effPsi_m2 = effectiveGrandCanonicalPotential();

                // fourth order approximation of the derivative
                double der = (-effPsi_p2 + 8.0*effPsi_p1 - 8.0*effPsi_m1 + effPsi_m2)/(12.0*inc);
                num_effStress(i,j) = der;
                if (i != j) num_effStress(i,j) *= 0.5;

                num_effStress(j,i) = num_effStress(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }
        // programmed effective stress
        istensor pr_effStress;
        stress(pr_effStress);

        // Compare effective stress with derivative of free energy wrt mu
        // relative error less than 0.01%
        istensor error = num_effStress - pr_effStress;

        isok = (error.norm()/pr_effStress.norm() < 1.0e-3);

        os << "\n   12. Comparing effective stress with [ d effective free energy / d eps].";
        if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in effective stress computation:       " <<  error.norm()/pr_effStress.norm();
            os << "\n      Programmed effective stress:                          " << pr_effStress;
            os << "\n      Numeric derivative of effective free energy wrt eps:  " << num_effStress;
        }
    }

    //13. concentrationDerivativeWrtEps test
    if (true)
    {
        // Compute numerical value of concentrationDerivativeWrtEps = d chi / d eps
        const double   inc = 1.0e-4;

        istensor num_chiDerWrtEps;
        num_chiDerWrtEps.setZero();

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double chi_p1 = concentration();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double chi_p2 = concentration();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double chi_m1 = concentration();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double chi_m2 = concentration();

                // fourth order approximation of the derivative
                double der = (-chi_p2 + 8.0*chi_p1 - 8.0*chi_m1 + chi_m2)/(12.0*inc);
                num_chiDerWrtEps(i,j) = der;
                if (i != j) num_chiDerWrtEps(i,j) *= 0.0;

                num_chiDerWrtEps(j,i) = num_chiDerWrtEps(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed concentrationDerivativeWrtEps
        istensor pr_chiDerWrtEps = concentrationDerivativeWrtEps();

        // Compare concentrationDerivativeWrtEps with derivative of chi wrt eps
        // relative error less than 0.01%
        istensor error = num_chiDerWrtEps - pr_chiDerWrtEps;

        isok = (error.norm()/pr_chiDerWrtEps.norm() < 1e-4);
        os << "\n   13. Comparing concentrationDerivativeWrtEps with [ d chi / d eps ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in concentrationDerivativeWrtEps computation %e " << error.norm()/pr_chiDerWrtEps.norm();
            os << "\n Programmed concentrationDerivativeWrtEps: " << pr_chiDerWrtEps;
            os << "\n Numeric derivative of chi wrt eps:        " << num_chiDerWrtEps;
        }
    }

    //14. effectiveConcentrationDerivativeWrtEps test
    if (true)
    {
        // Compute numerical value of effectiveConcentrationDerivativeWrtEps = d chieff / d eps
        const double   inc = 1.0e-4;

        istensor num_effChiDerWrtEps;
        num_effChiDerWrtEps.setZero();

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effChi_p1 = effectiveConcentration();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effChi_p2 = effectiveConcentration();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effChi_m1 = effectiveConcentration();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effChi_m2 = effectiveConcentration();

                // fourth order approximation of the derivative
                double der = (-effChi_p2 + 8.0*effChi_p1 - 8.0*effChi_m1 + effChi_m2)/(12.0*inc);
                num_effChiDerWrtEps(i,j) = der;
                if (i != j) num_effChiDerWrtEps(i,j) *= 0.0;

                num_effChiDerWrtEps(j,i) = num_effChiDerWrtEps(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed effective concentrationDerivativeWrtEps
        istensor pr_effChiDerWrtEps = effectiveConcentrationDerivativeWrtEps();

        // Compare effectiveConcentrationDerivativeWrtEps with derivative of chi eff wrt eps
        // relative error less than 0.01%
        istensor error = num_effChiDerWrtEps - pr_effChiDerWrtEps;

        isok = (error.norm()/pr_effChiDerWrtEps.norm() < 1e-4);
        os << "\n   14. Comparing effectiveConcentrationDerivativeWrtEps with [ d chi eff / d eps ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effectiveConcentrationDerivativeWrtEps computation %e " << error.norm()/pr_effChiDerWrtEps.norm();
            os << "\n Programmed effectiveConcentrationDerivativeWrtEps: " << pr_effChiDerWrtEps;
            os << "\n Numeric derivative of chi eff wrt eps:        " << num_effChiDerWrtEps;
        }
    }

    //15. chemicalCapacityDerivativeWrtEps test
    if (true)
    {
        // Compute numerical value of chemicalCapacityDerivativeWrtEps = d c / d eps
        const double   inc = 1.0e-4;

        istensor num_cDerWrtEps;
        num_cDerWrtEps.setZero();

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double c_p1 = chemicalCapacity();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double c_p2 = chemicalCapacity();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double c_m1 = chemicalCapacity();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double c_m2 = chemicalCapacity();

                // fourth order approximation of the derivative
                double der = (-c_p2 + 8.0*c_p1 - 8.0*c_m1 + c_m2)/(12.0*inc);
                num_cDerWrtEps(i,j) = der;
                if (i != j) num_cDerWrtEps(i,j) *= 0.0;

                num_cDerWrtEps(j,i) = num_cDerWrtEps(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed chemicalCapacityDerivativeWrtEps
        istensor pr_cDerWrtEps = chemicalCapacityDerivativeWrtEps();

        // Compare chemicalCapacityDerivativeWrtEps with derivative of c wrt eps
        // relative error less than 0.01%
        istensor error = num_cDerWrtEps - pr_cDerWrtEps;

        isok = (error.norm()/pr_cDerWrtEps.norm() < 1e-4);
        os << "\n   15. Comparing chemicalCapacityDerivativeWrtEps with [ d c / d eps ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in chemicalCapacityDerivativeWrtEps computation %e " << error.norm()/pr_cDerWrtEps.norm();
            os << "\n Programmed chemicalCapacityDerivativeWrtEps: " << pr_cDerWrtEps;
            os << "\n Numeric derivative of c wrt eps:             " << num_cDerWrtEps;
        }
    }

    //16. effectiveChemicalCapacityDerivativeWrtEps test
    if (true)
    {
        // Compute numerical value of effectiveChemicalCapacityDerivativeWrtEps = d c eff / d eps
        const double   inc = 1.0e-4;

        istensor num_effCDerWrtEps;
        num_effCDerWrtEps.setZero();

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effC_p1 = effectiveChemicalCapacity();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effC_p2 = effectiveChemicalCapacity();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effC_m1 = effectiveChemicalCapacity();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double effC_m2 = effectiveChemicalCapacity();

                // fourth order approximation of the derivative
                double der = (-effC_p2 + 8.0*effC_p1 - 8.0*effC_m1 + effC_m2)/(12.0*inc);
                num_effCDerWrtEps(i,j) = der;
                if (i != j) num_effCDerWrtEps(i,j) *= 0.0;

                num_effCDerWrtEps(j,i) = num_effCDerWrtEps(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed effectiveChemicalCapacityDerivativeWrtEps
        istensor pr_effCDerWrtEps = effectiveChemicalCapacityDerivativeWrtEps();

        // Compare effectiveChemicalCapacitytDerivativeWrtEps with derivative of c eff wrt eps
        // relative error less than 0.01%
        istensor error = num_effCDerWrtEps - pr_effCDerWrtEps;

        isok = (error.norm()/pr_effCDerWrtEps.norm() < 1e-4);
        os << "\n   16. Comparing effectiveChemicalCapacityDerivativeWrtEps with [ d c eff / d eps ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effectiveChemicalCapacityDerivativeWrtEps computation %e " << error.norm()/pr_effCDerWrtEps.norm();
            os << "\n Programmed effectiveChemicalCapacityDerivativeWrtEps: " << pr_effCDerWrtEps;
            os << "\n Numeric effective derivative of c wrt eps:             " << num_effCDerWrtEps;
        }
    }

    // 17. massFluxDerivativeWrtEps test
    if (true)
    {
        // Compute numerical value of massFluxDerivativeWrtEps = d j / d eps
        const double   inc = 1.0e-4;

        ivector num_jDerWrtEps;
        num_jDerWrtEps.setZero();

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                ivector j_p1; massFlux(j_p1);

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                ivector j_p2; massFlux(j_p2);

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                ivector j_m1; massFlux(j_m1);

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                ivector j_m2; massFlux(j_m2);

                // fourth order approximation of the derivative
                num_jDerWrtEps = (-j_p2 + 8.0*j_p1 - 8.0*j_m1 + j_m2)/(12.0*inc);
                //num_jWrtEps(i,j) = der;
                //if (i != j) num_jWrtEps(i,j) *= 0.5;

                //num_jWrtEps(j,i) = num_jWrtEps(i,j);
                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed massFluxDerivativeWrtEps
        ivector pr_jDerWrtEps; massFluxDerivativeWrtEps(pr_jDerWrtEps);

        // compare massFluxDerivativeWrtEps with derivative of massFlux wrt eps
        // relative error less than 0.01%
        ivector error = num_jDerWrtEps - pr_jDerWrtEps;

        isok = (error.norm()/pr_jDerWrtEps.norm() < 1e-4);

        os << "\n   17. Comparing massFluxDerivativeWrtEps with [d massFlux / d eps ].";

        if (pr_jDerWrtEps.norm() < 1e-16 && num_jDerWrtEps.norm() < 1e-16)
        {
            os << " Test passed.";
        }

        else if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in massFluxDerivativeWrtEps computation %e " << error.norm()/pr_jDerWrtEps.norm();
            os << "\n Programmed mass flux derivative wrt eps: " << pr_jDerWrtEps;
            os << "\n Numeric mass flux derivative wrt eps:    " << num_jDerWrtEps;
        }
    }

    // 18. mobilityDerivativeWrtEps test
    if (true)
    {
        // Compute numerical value of mobilityDertivativeWrtEps = d m / d eps
        const double   inc = 1.0e-4;

        istensor num_mDerWrtEps;
        num_mDerWrtEps.setZero();

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double m_p1 = mobility();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double m_p2 = mobility();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double m_m1 = mobility();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                double m_m2 = mobility();

                // fourth order approximation of the derivative
                double der = (-m_p2 + 8.0*m_p1 - 8.0*m_m1 + m_m2)/(12.0*inc);
                num_mDerWrtEps(i,j) = der;
                if (i != j) num_mDerWrtEps(i,j) *= 0.5;

                num_mDerWrtEps(j,i) = num_mDerWrtEps(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed mobilityDerivativeWrtEps
        istensor pr_mDerWrtEps = mobilityDerivativeWrtEps();

        // compare mobilityDerivativeWrtEps with derivative of mobility wrt eps
        // relative error less than 0.01%
        istensor error = num_mDerWrtEps - pr_mDerWrtEps;

        isok = (error.norm()/pr_mDerWrtEps.norm() < 1e-4);
        os << "\n   18. Comparing mobilityDerivativeWrtEps with [d m / d eps ].";

        if (pr_mDerWrtEps.norm() < 1e-16 && num_mDerWrtEps.norm() < 1e-16)
        {
            os << " Test passed.";
        }

        else if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in mobilityDerivativeWrtEps computation %e " << error.norm()/pr_mDerWrtEps.norm();
            os << "\n Programmed mobility derivative wrt eps: " << pr_mDerWrtEps;
            os << "\n Numeric mobility derivative wrt eps:    " << num_mDerWrtEps;
        }
    }

    //19. stressTangentTensor test
    if (true)
    {// Compute numerical value of stressTangentTensor = d stress / d eps
        const double   inc = 1.0e-3;

        itensor4 num_stressTangentTensor;
        num_stressTangentTensor.setZero();

        istensor dsigma, sigma_p1, sigma_p2, sigma_m1, sigma_m2;

        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigma_p1);

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigma_p2);

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigma_m1);

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                stress(sigma_m2);

                // fourth order approximation of the derivative
                dsigma = (-sigma_p2 + 8.0*sigma_p1 - 8.0*sigma_m1 + sigma_m2)/(12.0*inc);

                if (i != j) dsigma *= 0.5;

                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        num_stressTangentTensor(k,l,i,j) = dsigma(k,l);
                        num_stressTangentTensor(k,l,j,i) = dsigma(k,l);
                    }

                eps(i,j) = original;
                eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed stressTangentTensor
        itensor4 pr_stressTangentTensor; stressTangentTensor(pr_stressTangentTensor);

        // relative error less than 0.01%
        double error = 0.0;
        double norm = 0.0;
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        error += pow(num_stressTangentTensor(i,j,k,l)-pr_stressTangentTensor(i,j,k,l),2);
                        norm  += pow(pr_stressTangentTensor(i,j,k,l),2);
                    }
        error = sqrt(error);
        norm = sqrt(norm);
        isok = (error/norm < 1e-4);

        os << "\n   19. Comparing stressTangentTensor wwith [d stress / d eps].";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in stressTangentTensor computation %e " << error/norm;
            os << "\n Programmed stressTangentTensor:    " << pr_stressTangentTensor;
            os << "\n Numeric stress derivative wrt eps: " << num_stressTangentTensor;
        }
    }

    //20. effectiveStressTangentTensor test
    if (true)
    {// Compute numerical value of effectiveStressTangentTensor = d stress eff / d eps
        const double   inc = 1.0e-3;

        itensor4 num_effStressTangentTensor;
        num_effStressTangentTensor.setZero();

        istensor dEffSigma, effSigma_p1, effSigma_p2, effSigma_m1, effSigma_m2;

        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                effectiveStress(effSigma_p1);

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                effectiveStress(effSigma_p2);

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                effectiveStress(effSigma_m1);

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
                effectiveStress(effSigma_m2);

                // fourth order approximation of the derivative
                dEffSigma = (-effSigma_p2 + 8.0*effSigma_p1 - 8.0*effSigma_m1 + effSigma_m2)/(12.0*inc);

                if (i != j) dEffSigma *= 0.5;

                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        num_effStressTangentTensor(k,l,i,j) = dEffSigma(k,l);
                        num_effStressTangentTensor(k,l,j,i) = dEffSigma(k,l);
                    }

                eps(i,j) = original;
                eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, mu_c, gradMu_c);
            }
        }

        // programmed effectiveStressTangentTensor
        itensor4 pr_effStressTangentTensor; effectiveStressTangentTensor(pr_effStressTangentTensor);

        // relative error less than 0.01%
        double error = 0.0;
        double norm = 0.0;
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        error += pow(num_effStressTangentTensor(i,j,k,l)-pr_effStressTangentTensor(i,j,k,l),2);
                        norm  += pow(pr_effStressTangentTensor(i,j,k,l),2);
                    }
        error = sqrt(error);
        norm = sqrt(norm);
        isok = (error/norm < 1e-4);

        os << "\n   20. Comparing effectiveStressTangentTensor wwith [d stress eff / d eps].";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effectiveStressTangentTensor computation %e " << error/norm;
            os << "\n Programmed effectiveStressTangentTensor:    " << pr_effStressTangentTensor;
            os << "\n Numeric effective stress derivative wrt eps: " << num_effStressTangentTensor;
        }
    }

    // Derivatives with respect to gradMu
    //21. massFlux; 22. mobility;

    //21. massFlux test
    if (true)
    {
        // Compute numerical value of mass flux massFlux = -(d kineticPotential / d gradMu)
        ivector num_massFlux;
        num_massFlux.setZero();
        const double inc = 1.0e-4;

        for (size_t i=0; i<3; i++)
        {
            double original = gradMu(i);

            gradMu(i) = original + inc;
            theMP.updateCurrentState(tn1, eps, mu, gradMu);
            double Omega_p1 = diffusionPotential();

            gradMu(i) = original + 2.0*inc;
            theMP.updateCurrentState(tn1, eps, mu, gradMu);
            double Omega_p2 = diffusionPotential();

            gradMu(i) = original - inc;
            theMP.updateCurrentState(tn1, eps, mu, gradMu);
            double Omega_m1 = diffusionPotential();

            gradMu(i) = original - 2.0*inc;
            theMP.updateCurrentState(tn1, eps, mu, gradMu);
            double Omega_m2 = diffusionPotential();


            // fourth order approximation of the derivative
            double der = (-Omega_p2 + 8.0*Omega_p1 - 8.0*Omega_m1 + Omega_m2)/(12.0*inc);
            num_massFlux(i) = der;

            gradMu(i) = original;
        }

        // programmed mass flux
        ivector pr_massFlux; massFlux(pr_massFlux);

        // compare mass flux with derivative of kinetic potential wrt gradMu

        // relative error less than 0.01%
        ivector error = num_massFlux - pr_massFlux;
        isok = (error.norm()/pr_massFlux.norm() < 1e-3);
        os << "\n   21. Comparing massFlux with - [d Omega / d gradMu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in massFlux computation %e." <<  error.norm()/pr_massFlux.norm();
            os << "\n Programmed mass flux: " << pr_massFlux;
            os << "\n Numeric mass flux:    " << num_massFlux;

        }
    }

    //22. mobility test
    if (true)
    {
        // Compute numerical value (approximation of derivative) of mobility  m = -(d j / d gradMu)
        const double inc = 1.0e-3;

        const size_t nnumder = 4;
        const double ndtimes[] = {+1.0, +2.0, -1.0, -2.0};
        const double ndfact[]  = {+8.0, -1.0, -8.0, +1.0};
        const double ndden = 12.0;
        istensor num_m;
        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=0; j<3; j++)
            {
                double der = 0.0;
                for (unsigned q=0; q<nnumder; q++)
                {
                    ivector gmu = gradMu_n;
                    gmu[j] = gradMu_n[j] + inc*ndtimes[q];
                    theMP.updateCurrentState(tn1, eps, mu_n, gmu);

                    ivector massFlux_aux; massFlux(massFlux_aux);
                    der += ndfact[q]*massFlux_aux(i);
                    theMP.resetCurrentState();
                }
                der /= inc*ndden;
                num_m(i,j) = -der;
            }
        }

        // compare programmed mobility with numerical derivative of mass flux wrt gradMu
        istensor pr_m = mobility()*istensor::identity();

        double error = (num_m - pr_m).norm();
        isok = (fabs(error)/pr_m.norm() < 1e-3);

        os << "\n   22. Comparing mobility with [d massFlux / d gradMu ].";
        if (isok)
        {
            os << " Test passed.";
        }

        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in mobility computation %e." <<  fabs(error)/pr_m.norm();
            os << "\n Programmed mobility: " << pr_m;
            os << "\n Numeric mobility:    " << num_m;

        }
    }

    // 23. contractWithTangent test
    if (true)
    {
        itensor Tvw;
        ivector v, w;
        v.setRandom();
        w.setRandom();
        contractWithTangent(v, w, Tvw);

        istensor S; S.setRandom();
        itensor4 tg; stressTangentTensor(tg);

        itensor nTvw; nTvw.setZero();
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nTvw(i,k) += tg(i,j,k,l)*v(j)*w(l);
                    }

        // relative error less than 0.01%
        itensor error = Tvw - nTvw;
        isok = (error.norm()/Tvw.norm() < 1e-4);

        os << "\n   23. Comparing contract tangent with C_ijkl v_j w_l.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error: " << error.norm()/Tvw.norm();
            os << "\n      C{a,b} \n" << Tvw;
            os << "\n      C_ijkl a_j b_l:\n" << nTvw;
        }
    }

    return isok;
}




double sMechMassHydMP::thetal() const
{
    const double nl     = theMechMassHydMaterial.nl;

    return concentrationl()/nl;
}




double sMechMassHydMP::thetat() const
{
    return concentrationt()/nt();
}




double sMechMassHydMP::effectiveThetat() const
{
    const double K_T     = theMechMassHydMaterial.kt();
    double       thetaL = thetal();

    return K_T*thetaL / (1.0 - thetaL + K_T*thetaL);
}




void sMechMassHydMP::updateCurrentState(const double t, const istensor& strain, const double mu, const ivector& gradMu)
{
    mu_c     = mu;
    gradMu_c = gradMu;

    theSSMP->updateCurrentState(t, strain);
}




double sMechMassHydMP::volumetricEnergy() const
{
    return theSSMP->volumetricEnergy();
}




double sMechMassHydMP::volumetricStiffness() const
{
    return theSSMP->volumetricStiffness();
}

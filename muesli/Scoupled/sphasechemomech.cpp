/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include <stdio.h>
#include "sphasechemomech.h"
#include "muesli/Smallstrain/smallstrainlib.h"

using namespace muesli;


sPhaseChemoMechMaterial::sPhaseChemoMechMaterial(const std::string& name,
                                                 const materialProperties& cl)
:
material(name, cl),
theSSMaterial(0),
ell(1.0),
gc(1.0),
diff(0.0)
{
    if       (cl.find("subtype elastic") != cl.end())      theSSMaterial = new elasticIsotropicMaterial(name, cl);
    else if  (cl.find("subtype plastic") != cl.end())      theSSMaterial = new splasticMaterial(name, cl);
    else if  (cl.find("subtype viscoelastic") != cl.end()) theSSMaterial = new viscoelasticMaterial(name, cl);
    else if  (cl.find("subtype viscoplastic") != cl.end()) theSSMaterial = new viscoplasticMaterial(name, cl);

    muesli::assignValue(cl, "ell", ell);
    muesli::assignValue(cl, "gc",  gc);
    muesli::assignValue(cl, "diffusivity", diff);
}




sPhaseChemoMechMaterial::~sPhaseChemoMechMaterial()
{
    if (theSSMaterial != 0) delete(theSSMaterial);
}




bool sPhaseChemoMechMaterial::check() const
{
    return theSSMaterial->check();
}




sPhaseChemoMechMP* sPhaseChemoMechMaterial::createMaterialPoint() const
{
    return new sPhaseChemoMechMP(*this);
}




double sPhaseChemoMechMaterial::density() const
{
    return theSSMaterial->density();
}




double sPhaseChemoMechMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_CHAR_LENGTH:    ret = ell; break;
        case PR_G_CRITICAL:     ret = gc; break;
        case PR_DIFFUSIVITY:    ret = diff; break;
        default:
            ret = theSSMaterial->getProperty(p);
    }
    return ret;
}




void sPhaseChemoMechMaterial::print(std::ostream &of) const
{
    of  << "\n Small strain chemo-mechanical material with phase field fracture."
        << "\n Surrogate mechanical model: ";
    theSSMaterial->print(of);

    of  << "\n Characteristic length:           " << ell
        << "\n Critical energy release rate Gc: " << gc
        << "\n Mass diffusivity:                " << diff;
}




void sPhaseChemoMechMaterial::setRandom()
{
    int mattype = discreteUniform(1, 1);
    std::string name = "surrogate small strain material";
    materialProperties mp;

    if (mattype == 0)
        theSSMaterial = new elasticIsotropicMaterial(name, mp);

    else if (mattype == 1)
        theSSMaterial = new splasticMaterial(name, mp);

    else if (mattype == 2)
        theSSMaterial = new viscoelasticMaterial(name, mp);

    else if (mattype == 3)
        theSSMaterial = new viscoplasticMaterial(name, mp);

    else if (mattype == 4)
        theSSMaterial = new GTN_Material(name, mp);

    theSSMaterial->setRandom();
    ell  = randomUniform(1.0, 2.0);
    gc   = randomUniform(100.0, 400.0);
    diff = randomUniform(1.0, 2.0);
}




bool sPhaseChemoMechMaterial::test(std::ostream& of)
{
    bool isok = true;
    setRandom();

    sPhaseChemoMechMP* p = this->createMaterialPoint();

    isok = p->testImplementation(of);
    delete p;

    return isok;
}




double sPhaseChemoMechMaterial::waveVelocity() const
{
    return theSSMaterial->waveVelocity();
}




sPhaseChemoMechMP::sPhaseChemoMechMP(const sPhaseChemoMechMaterial &m)
:
  phase_n(0.0), phase_c(0.0),
  theSSMP(nullptr),
  thePhaseChemoMechMaterial(m)
{
    theSSMP = m.theSSMaterial->createMaterialPoint();
    gradPh_n.setZero();
    gradPh_c.setZero();
}




sPhaseChemoMechMP::~sPhaseChemoMechMP()
{
    if (theSSMP != 0) delete (theSSMP);
}




void sPhaseChemoMechMP::commitCurrentState()
{
    phase_n = phase_c;
    gradPh_n = gradPh_c;
    theSSMP->commitCurrentState();
}




double sPhaseChemoMechMP::contractWithConductivity(const ivector &v1, const ivector &v2) const
{
    return thePhaseChemoMechMaterial.diffusivity * v1.dot(v2);
}




void sPhaseChemoMechMP::contractWithTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithTangent(v1, v2, T);
}




void sPhaseChemoMechMP::contractWithDeviatoricTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithDeviatoricTangent(v1, v2, T);
}




double sPhaseChemoMechMP::deviatoricEnergy() const
{
    return theSSMP->deviatoricEnergy();
}




void sPhaseChemoMechMP::deviatoricStress(istensor& s) const
{
    theSSMP->deviatoricStress(s);
}




double sPhaseChemoMechMP::energyDissipationInStep() const
{
    // mechanical part
    double dissMech   = theSSMP->energyDissipationInStep();

    return dissMech;
}




// II - Cinverse:Cep for small sPhaseChemoMech element
void sPhaseChemoMechMP::dissipationTangent(itensor4& D) const
{
    theSSMP -> dissipationTangent(D);
}




double sPhaseChemoMechMP::effectiveStoredEnergy() const
{
    return 0.0;
}




double sPhaseChemoMechMP::entropy() const
{
    return 0.0;
}





double sPhaseChemoMechMP::freeEnergy() const
{
    return 0.0;
}




istensor sPhaseChemoMechMP::getConvergedPlasticStrain() const
{
    return theSSMP->getConvergedPlasticStrain();
}




materialState sPhaseChemoMechMP::getConvergedState() const
{
    materialState mat = theSSMP->getConvergedState();
    mat.theDouble.push_back(phase_n);
    mat.theVector.push_back(gradPh_n);

    return mat;
}




istensor sPhaseChemoMechMP::getCurrentPlasticStrain() const
{
    return theSSMP->getCurrentPlasticStrain();
}




materialState sPhaseChemoMechMP::getCurrentState() const
{
    // first store the state of the ssmp
    materialState mat = theSSMP->getCurrentState();

    mat.theDouble.push_back(phase_c);
    mat.theVector.push_back(gradPh_c);

    return mat;
}




istensor& sPhaseChemoMechMP::getCurrentStrain()
{
    return theSSMP->getCurrentState().theStensor[0];
}




ivector sPhaseChemoMechMP::heatflux() const
{
    return gradPh_c;
}




double sPhaseChemoMechMP::plasticSlip() const
{
    return theSSMP->plasticSlip();
}




double sPhaseChemoMechMP::pressure() const
{
    return theSSMP->pressure();
}




void sPhaseChemoMechMP::resetCurrentState()
{
    phase_c = phase_n;
    gradPh_c = gradPh_n;
    theSSMP->resetCurrentState();
}




void sPhaseChemoMechMP::setRandom()
{
    phase_c = randomUniform(0.1, 1.0);
    gradPh_c.setRandom();
    theSSMP->setRandom();
}




void sPhaseChemoMechMP::stress(istensor& sigma) const
{

}




void sPhaseChemoMechMP::stressTemperatureTensor(istensor& M) const
{
 }




void sPhaseChemoMechMP::tangentElasticities(itensor4& c) const
{
    theSSMP->tangentTensor(c);
}




double& sPhaseChemoMechMP::phaseField()
{
    return phase_c;
}




const double& sPhaseChemoMechMP::phaseField() const
{
    return phase_c;
}




bool sPhaseChemoMechMP::testImplementation(std::ostream& os) const
{
    return true;
}




void sPhaseChemoMechMP::updateCurrentState(const double t, const istensor& strain, const double phase, const ivector& gradphi)
{
    phase_c  = phase;
    gradPh_c = gradphi;
    theSSMP->updateCurrentState(t, strain);
}




double sPhaseChemoMechMP::volumetricEnergy() const
{
    return theSSMP->volumetricEnergy();
}




double sPhaseChemoMechMP::volumetricStiffness() const
{
    return theSSMP->volumetricStiffness();
}

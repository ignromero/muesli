/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#pragma once
#ifndef _muesli_material_
#define _muesli_material_

#include <stdio.h>
#include <map>
#include <vector>
#include <iostream>
#include <string>
#include "muesli/tensor.h"
#include "muesli/Utils/utils.h"


namespace muesli
{
    class materialPoint;

    class material
    {

    public:
                                    material();
                                    material(const std::string& name);
                                    material(const std::string& name,
                                             const materialProperties& cl);
        virtual                     ~material();

        virtual bool                check() const;
        virtual double              getProperty(const propertyName p) const=0;
        virtual void                print(std::ostream &of=std::cout) const=0;
        virtual void                setRandom();
        virtual void                setDensity(double rho){_rho = rho;}
        virtual void                setReferenceTemperature(double tt){_refTemp=tt;}


        // this is the log file for all material data
        static void                 setLogger(std::ostream &of);
        static std::ostream&        getLogger();


        // information
        inline virtual double       density() const;
        inline bool                 isVariational() const;
        virtual void                setVariational();
        inline size_t               label() const;
        inline const std::string&   name() const;
        inline double               referenceTemperature() const;


    private:
        static std::ostream*        materialLog;
        std::string                 _name;
        size_t                      _label;
        double                      _rho, _refTemp;
        bool                        _variationalFormulation;
    };


    // inlines
    inline double                   material::density() const {return _rho;}
    inline size_t                   material::label() const {return _label;}
    inline bool                     material::isVariational() const { return _variationalFormulation;}
    inline void                     material::setVariational() {_variationalFormulation=true;}
    inline const std::string&       material::name() const {return _name;}
    inline double                   material::referenceTemperature() const {return _refTemp;}



    class materialPoint
    {
    public:
                                    materialPoint();
                                    materialPoint(material &m);
        virtual                     ~materialPoint(){};

        virtual void                commitCurrentState()=0;
        virtual double              density() const = 0;
        virtual void                getStateVariable(stateVariableName n, stateVariable& v) const;
        virtual materialState       getConvergedState() const = 0;
        virtual materialState       getCurrentState() const = 0;
        virtual void                resetCurrentState()=0;
    };


    // the materialDB is a way to keep all the materials
    // in a single db which has convenient access
    // this is a singleton class
    class materialDB : public std::map<size_t, muesli::material*>
    {
    public:
                                    materialDB(materialDB const& copy) = delete;
        materialDB&                 operator=(materialDB const& copy) = delete;

        static void                 addMaterial(const size_t label, muesli::material& m);
        static muesli::material&    getMaterial(const size_t label);
        static muesli::material&    getMaterial(const std::string& name);
        static void                 free();
        static void                 print(std::ostream &of=std::cout);

    private:
        static materialDB&          getDB();
                                    materialDB(){}
    };
}

#endif

/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#include "muesli/Math/mtensor.h"
#include "muesli/Utils/utils.h"
#define _USE_MATH_DEFINES

#include <cassert>
#include <cmath>
#include <iomanip>
#include <string.h>
#include "lemaitre.h"

#define SQ23 0.81649568092
#define J2TOL 1e-7
#define GAMMAITER 20
#define THIRD 0.3333333333333

using namespace muesli;
using namespace std;

FLemaitreMaterial::FLemaitreMaterial(const std::string& name,
                                   const materialProperties& cl)
:
  fplasticMaterial(name, cl),
  E(0.0), nu(0.0), lambda(0.0), mu(0.0), bulk(0.0), cp(0.0), cs(0.0),
  Y0(0.0), Yinf(-1.0), Yexp(0.0), soft(0.0), Hiso(0.0), Hkine(0.0)
{
    muesli::assignValue(cl, "young",       E);
    muesli::assignValue(cl, "poisson",     nu);
    muesli::assignValue(cl, "bulk",        bulk);
    muesli::assignValue(cl, "lambda",      lambda);
    muesli::assignValue(cl, "mu",          mu);
    muesli::assignValue(cl, "isotropich",  Hiso);
    muesli::assignValue(cl, "kinematich",  Hkine);
    muesli::assignValue(cl, "yieldstress", Y0);
    muesli::assignValue(cl, "yieldinf",    Yinf);
    muesli::assignValue(cl, "hardexp",     Yexp);
    muesli::assignValue(cl, "softening",   soft);

    if (Yinf == -1.0) Yinf = Y0;

    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else if (bulk != 0.0)
    {
        lambda = bulk - 2.0/3.0*mu;
    }
    else
    {
        nu = 0.5 * lambda / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }

    // we set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;

    if (density() > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/density());
        cs = sqrt(mu/density());
    }
}




muesli::finiteStrainMP* FLemaitreMaterial::createMaterialPoint() const
{
    muesli::finiteStrainMP *mp = new FLemaitreMP(*this);
    return mp;
}




// this function is much faster than the one with string property names, because
// avoids string comparisons. It should be used.
double FLemaitreMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;

        default:
      std::cout << "\n Error in elasticIsotropicMaterial. Property not defined";
    }
    return ret;
}




void FLemaitreMaterial::print(std::ostream &of) const
{
    of  << "\n Elastoplastic material for finite strain kinematics."
        << "\n Von Mises yield criterion with linear kinematic hardening"
        << "\n and saturation isotropic hardening."
        << "\n   Young modulus:  E      : " << E
        << "\n   Poisson ratio:  nu     : " << nu
        << "\n   Lame constants: Lambda : " << lambda
        << "\n                   Mu     : " << mu
        << "\n   Bulk modulus:   K      : " << bulk
        << "\n   Density                : " << density()
        << "\n   Wave velocities C_p    : " << cp
        << "\n                   C_s    : " << cs
        << "\n   Yield stress           : " << Y0
        << "\n   Yield softening        : " << soft
        << "\n   Isotropic hardening    : " << Hiso
        << "\n   Kinematic hardening    : " << Hkine
        << "\n   Saturation yield stress: " << Yinf
        << "\n   Hardening exponent     : " << Yexp
        << "\n   Reference temperature  : " << referenceTemperature()
        << "\n   Variational basis      : " << isVariational();
}




void FLemaitreMaterial::setRandom()
{
    E      = muesli::randomUniform(1.0e4, 1.0e5);
    nu     = muesli::randomUniform(0.05, 0.45);
    setDensity(muesli::randomUniform(1.0, 100.0));
    Y0     = 0.001*E*muesli::randomUniform(0.5, 1.5);
    soft   = randomUniform(0.0, 1.0)*0.0;
    Yinf   = Y0*muesli::randomUniform(1.5, 2.5);
    Yexp   = muesli::randomUniform(0.9, 1.1)*0.0;
    Hiso   = E*1e-4 * muesli::randomUniform(1.0, 2.0);
    Hkine  = E*1e-4 * muesli::randomUniform(1.0, 2.0);

    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/density());
    cs     = sqrt(2.0*mu/density());
    bulk   = lambda + 2.0/3.0 * mu;
}




FLemaitreMP::FLemaitreMP(const FLemaitreMaterial &m)
:
muesli::fplasticMP(m),
theFLemaitreMaterial(m),
iso_n(0.0), iso_c(0.0),
dgamma(0.0),
tgs(0.0), tgdelta(0.0)
{
    be_n = be_c = istensor::identity();
    kine_n.setZero();
    kine_c.setZero();
    tau.setZero();
    lambda2 = lambda2TR = ivector(1.0, 1.0, 1.0);
    tau.setZero();
    nubarTR.setZero();
    nn[0] = ivector(1.0, 0.0, 0.0);
    nn[1] = ivector(0.0, 1.0, 0.0);
    nn[2] = ivector(0.0, 0.0, 1.0);
}




void FLemaitreMP::CauchyStress(istensor& sigma) const
{
    /*const double iJ = 1.0/Jc;
    istensor kir; KirchhoffStress(kir);
    sigma = iJ*kir;*/
}




void FLemaitreMP::commitCurrentState()
{
    fplasticMP::commitCurrentState();

    be_n   = be_c;
    kine_n = kine_c;
    iso_n  = iso_c;
}



/*
void FLemaitreMP::contractWithConvectedTangent(const ivector &v1, const ivector& v2, itensor &T) const
{
}

void FLemaitreMP::contractWithSpatialTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
}*/




void FLemaitreMP::convectedTangent(itensor4& C) const
{
    itensor4 Cs;
    spatialTangent(Cs);

    C.setZero();
    itensor Finv = Fc.inverse();
    for (size_t i=0; i < 3; i++)
        for (size_t j=0; j < 3; j++)
            for (size_t k=0; k < 3; k++)
                for (size_t l=0; l < 3; l++)
                    for (size_t m=0; m < 3; m++)
                        for (size_t n=0; n < 3; n++)
                            for (size_t p=0; p < 3; p++)
                                for (size_t q=0; q < 3; q++)
                                    C(i,j,k,l) +=  Jc * Finv(i,m)*Finv(j,n)*Finv(k,p)*Finv(l,q)*Cs(m,n,p,q);
}




double FLemaitreMP::deviatoricEnergy() const
{
    const double mu = theFLemaitreMaterial.mu;

    ivector epse;
    for (size_t i=0; i<3; i++)
        epse[i] = 0.5*log(lambda2[i]);

    double We = mu * epse.squaredNorm();

    return We;
}




// d Diss / d F
itensor FLemaitreMP::dissipatedEnergyDF() const
{
    itensor dDdF;
    return dDdF;
}




double FLemaitreMP::dissipatedEnergyDTheta() const
{
    return 0;
}




double FLemaitreMP::energyDissipationInStep() const
{
    double d = 0.0;

    if (dgamma > 0.0)
    {
        const double Y00   = theFLemaitreMaterial.Y0;
        const double soft  = theFLemaitreMaterial.soft;
        const double temp0 = theFLemaitreMaterial.referenceTemperature();
        const double Y0    = Y00*exp(-soft*(tempc-temp0));
        const double vScale = variationalScale();
        d = SQ23*Y0*dgamma*vScale;
    }

    return d;
}




double FLemaitreMP::effectiveStoredEnergy() const
{
    return storedEnergy() + energyDissipationInStep();
}




void FLemaitreMP::firstPiolaKirchhoffStress(itensor &P) const
{
    istensor S; secondPiolaKirchhoffStress(S);
    P = Fc*S;
}




double FLemaitreMP::getDamage() const
{
    const double soft   = theFLemaitreMaterial.soft;
    const double temp0  = theFLemaitreMaterial.referenceTemperature();
    const double Y00    = theFLemaitreMaterial.Y0;
    const double Y0     = Y00*exp(-soft*(tempc-temp0));
    const double Hiso0  = theFLemaitreMaterial.Hiso;
    const double Hiso   = Hiso0*exp(-soft*(tempc-temp0));

    return Y0 + Hiso*iso_c;
}




double FLemaitreMP::kineticPotential() const
{
    const ivector vone(1.0, 1.0, 1.0);
    const double vScale = variationalScale();
    const double temp0 = theFLemaitreMaterial.referenceTemperature();
    const double mu    = theFLemaitreMaterial.mu;
    const double Hkine = theFLemaitreMaterial.Hkine;
    const double Y00   = theFLemaitreMaterial.Y0;
    const double soft  = theFLemaitreMaterial.soft;
    const double Y0    = Y00*exp(-soft*(tempc-temp0)) * vScale;
    const double Hiso0 = theFLemaitreMaterial.Hiso;
    const double Hiso  = Hiso0*exp(-soft*(tempc-temp0));
    const double Yinf  = theFLemaitreMaterial.Yinf;
    const double Yexp  = theFLemaitreMaterial.Yexp;

    // logarithmic principal elastic stretches
    ivector  neigvec[3], lambda2n;;
    be_n.spectralDecomposition(neigvec, lambda2n);
    ivector epse_c, epse_n;
    for (size_t i=0; i<3; i++)
    {
        epse_c[i] = 0.5*log(lambda2[i]);
        epse_n[i] = 0.5*log(lambda2n[i]);
    }

    ivector devepse_c, devepse_n;
    //devepse_c = epse_c - M_THIRD * epse_c.dot(vone)*vone;
    //devepse_n = epse_n - M_THIRD * epse_n.dot(vone)*vone;

    ivector s = 2.0 * mu * devepse_c;
    ivector Q = -2.0/3.0 * Hkine * kine_c;
    double  q = -Hiso*iso_c - (Yinf - Y0)*(1.0-exp(-Yexp*iso_c));
    ivector dt_dp = dgamma*nubarTR;
    double  dt_Psistar = dt_dp.dot(s) + (kine_c - kine_n).dot(Q) + (iso_c - iso_n)*q;

    double dt = tc - tn;

    double Psistar = (dt == 0.0) ? 0.0 : dt_Psistar/dt;
    //return energyDissipationInStep()/dt;
    return Psistar;
}




void FLemaitreMP::KirchhoffStress(istensor &ttau) const
{
    ttau.setZero();
    for (size_t i=0; i<3; i++)
    {
        ttau.addScaledVdyadicV(tau[i], nn[i]);
    }
}




double FLemaitreMP::plasticSlip() const
{
    return iso_c;
}




// beta: deviatoric, shifted principal Kirchhoff stresses
// xi  : isotropic hardening strain-like internal variable
double FLemaitreMP::radialReturn(const ivector& beta, const double& xi) const
{
    const double vScale = variationalScale();
    const double temp0 = theFLemaitreMaterial.referenceTemperature();
    const double mu    = theFLemaitreMaterial.mu;
    const double Hkine = theFLemaitreMaterial.Hkine;
    const double Y00   = theFLemaitreMaterial.Y0;
    const double soft  = theFLemaitreMaterial.soft;
    const double Y0    = Y00*exp(-soft*(tempc-temp0)) * vScale;
    const double Hiso0 = theFLemaitreMaterial.Hiso;
    const double Hiso  = Hiso0*exp(-soft*(tempc-temp0));
    const double Yinf  = theFLemaitreMaterial.Yinf;
    const double Yexp  = theFLemaitreMaterial.Yexp;
    const double bnorm = beta.norm();

    double dg   = 0.0;
    double G    = 1.0;
    double iso  = xi;
    size_t count = 0;

    double Kp, Kpp, DG;
    const double tol = J2TOL;

    while (G > (J2TOL*Y0) && ++count < GAMMAITER)
    {
        Kp  = Hiso*iso + (Yinf - Y0)*(1.0-exp(-Yexp*iso));
        Kpp = Hiso+(Yinf-Y0)*Yexp*exp(-Yexp*iso);
        G   = bnorm - SQ23*(Y0 + Kp)*(1.0+tol) - dg*(2.0*mu+2.0/3.0*Hkine);
        DG  = -2.0*mu -2.0/3.0*(Hkine + Kpp);
        dg -= G/DG;
        iso = xi + SQ23 * dg;
    }

    if (count == GAMMAITER)
    {
        std::cout << "\n error in radial return";
        dg = 0.0;
    }

    return dg;
}




void FLemaitreMP::resetCurrentState()
{
    finiteStrainMP::resetCurrentState();

    iso_c  = iso_n;
    kine_c = kine_n;
    be_c   = be_n;
}




void FLemaitreMP::secondPiolaKirchhoffStress(istensor &S) const
{
    istensor kir;
    KirchhoffStress(kir);
    S = istensor::FSFt(Fc.inverse(), kir);
}




void FLemaitreMP::setConvergedState(const double theTime, const itensor& F,
                                   const double iso, const ivector& kine,
                                   const istensor& be)
{
    tn     = theTime;
    Fn     = F;
    Jn     = Fn.determinant();
    iso_n  = iso;
    kine_n = kine;
    be_n   = be;
}




void FLemaitreMP::setRandom()
{
    iso_c = iso_n = muesli::randomUniform(1.0, 2.0);

    ivector tmp; tmp.setRandom();
    ivector vone(1.0, 1.0, 1.0);
    kine_c = kine_n = tmp - 1.0/3.0*tmp.dot(vone) * vone;

    itensor Fe; Fe.setRandom();
    if (Fe.determinant() < 0.0) Fe *= -1.0;
    be_c = be_n = istensor::tensorTimesTensorTransposed(Fe);

    itensor Fp; Fp.setRandom();
    Fp *= 1.0/cbrt(Fp.determinant());
    Fc = Fn = Fe*Fp;
    Jc = Fc.determinant();

    tempn = muesli::randomUniform(0.9, 1.1) * theFiniteStrainMaterial.referenceTemperature();
    tempc = muesli::randomUniform(0.9, 1.1) * theFiniteStrainMaterial.referenceTemperature();
}




void FLemaitreMP::spatialTangent(itensor4& Cs) const
{
    const double mu    = theFLemaitreMaterial.mu;
    const double kappa = theFLemaitreMaterial.bulk;
    const ivector vone(1.0, 1.0, 1.0);
    const double TOL = 1e-10;
    const double iJ = 1.0/Fc.determinant();

    istensor cep;
    if (dgamma <= 0.0)
    {
        cep = istensor::scaledIdentity(2.0*mu);
        cep.addScaledVdyadicV(kappa-2.0/3.0*mu, vone);
    }
    else
    {
        cep = istensor::scaledIdentity(2.0*mu*tgs);
        cep.addScaledVdyadicV(kappa-2.0/3.0*mu*tgs, vone);
        cep.addScaledVdyadicV(-2.0*mu*tgdelta, nubarTR);
    }

    itensor G;
    for (unsigned a=0; a<3; a++)
    {
        G(a,a) = -2.0 * tau(a);
        for (unsigned b=0; b<a; b++)
        {
            if ( fabs(lambda2TR[a] - lambda2TR[b]) > TOL )
            {
                G(a,b) = G(b,a) = (tau[a]*lambda2TR[b] - tau[b]*lambda2TR[a])/(lambda2TR[a] - lambda2TR[b]);
            }
            else
            {
                G(a,b) = G(b,a) = 0.25*cep(a,a)+0.25*cep(b,b)-0.5*cep(a,b)-0.5*tau(a)-0.5*tau(b);
            }
        }
    }


    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<=i; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<=k; l++)
                {
                    Cs(i,j,k,l) = 0.0;
                    for (unsigned a=0; a<3; a++)
                        for(unsigned b=0; b<3; b++)
                        {
                            Cs(i,j,k,l) += iJ * cep(a,b) * nn[a][i]*nn[a][j]*nn[b][k]*nn[b][l];
                        }

                    for (unsigned a=0; a<3; a++)
                    {
                        Cs(i,j,k,l) += iJ * G(a,a) * nn[a][i]*nn[a][j]*nn[a][k]*nn[a][l];

                        for (unsigned b=0; b<a; b++)
                        {
                            Cs(i,j,k,l) += iJ * G(a,b) * nn[a][i]*nn[b][j]*nn[a][k]*nn[b][l]
                                        +  iJ * G(a,b) * nn[a][i]*nn[b][j]*nn[b][k]*nn[a][l];

                            Cs(i,j,k,l) += iJ * G(b,a) * nn[b][i]*nn[a][j]*nn[b][k]*nn[a][l]
                                        +  iJ * G(b,a) * nn[b][i]*nn[a][j]*nn[a][k]*nn[b][l];
                        }
                    }
                }

    for (unsigned j=1; j<3; j++)
        for (unsigned i=0; i<j; i++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<=k; l++)
                {
                    Cs(i,j,k,l) = Cs(j,i,k,l);
                }

    for (unsigned j=0; j<3; j++)
        for (unsigned i=0; i<3; i++)
            for (unsigned l=1; l<3; l++)
                for (unsigned k=0; k<l; k++)
                {
                    Cs(i,j,k,l) = Cs(i,j,l,k);
                }
}




double FLemaitreMP::storedEnergy() const
{
    const double lambda= theFLemaitreMaterial.lambda;
    const double mu    = theFLemaitreMaterial.mu;
    const double Hkine = theFLemaitreMaterial.Hkine;
    const double soft  = theFLemaitreMaterial.soft;
    const double temp0 = theFLemaitreMaterial.referenceTemperature();
    const double Y00   = theFLemaitreMaterial.Y0;
    const double Y0    = Y00*exp(-soft*(tempc-temp0));
    const double Hiso0 = theFLemaitreMaterial.Hiso;
    const double Hiso  = Hiso0*exp(-soft*(tempc-temp0));
    const double Yinf  = theFLemaitreMaterial.Yinf;
    const double Yexp  = theFLemaitreMaterial.Yexp;

    ivector epse;
    for (size_t i=0; i<3; i++)
        epse[i] = 0.5*log(lambda2[i]);

    double trace = epse(0) + epse(1) + epse(2);
    double We = 0.5 * lambda * trace * trace + mu * epse.squaredNorm();
    double Wp = THIRD * Hkine * kine_c.squaredNorm() + 0.5 * Hiso * iso_c*iso_c;

    if (Yexp != 0.0)
    {
        Wp += (Yinf-Y0) * (iso_c + exp(-Yexp*iso_c)/ Yexp - 1.0/Yexp) ;
    }

    return Wp+We;
}




void FLemaitreMP::updateCurrentState(const double theTime, const istensor& C)
{
    itensor U = istensor::squareRoot(C);
    this->updateCurrentState(theTime, U);
}




void FLemaitreMP::updateCurrentState(const double theTime, const itensor& F)
{
    finiteStrainMP::updateCurrentState(theTime, F);

    const double mu     = theFLemaitreMaterial.mu;
    const double kappa  = theFLemaitreMaterial.bulk;
    const double soft   = theFLemaitreMaterial.soft;
    const double temp0  = theFLemaitreMaterial.referenceTemperature();
    const double Y00    = theFLemaitreMaterial.Y0;
    const double Y0     = Y00*exp(-soft*(tempc-temp0));
    const double Hkin   = theFLemaitreMaterial.Hkine;
    const double Hiso0  = theFLemaitreMaterial.Hiso;
    const double Hiso   = Hiso0*exp(-soft*(tempc-temp0));
    const double Yinf   = theFLemaitreMaterial.Yinf;
    const double Yexp   = theFLemaitreMaterial.Yexp;
    const ivector vone(1.0, 1.0, 1.0);

    // Incremental deformation gradient f = F * Fn^-1 */
    const itensor f = F * Fn.inverse();

    //---------------------------------------------------------------------------------------
    //                                     trial state
    //---------------------------------------------------------------------------------------
    // trial elastic finger tensor b_e
    const istensor beTR = istensor::FSFt(f, be_n);

    // logarithmic principal elastic stretches
    beTR.spectralDecomposition(nn, lambda2TR);
    ivector epseTR;
    for (size_t i=0; i<3; i++) epseTR[i] = 0.5*log(lambda2TR[i]);
    const double theta = epseTR.dot(vone);

    // trial state with frozen plastic flow
    const ivector devepseTR = epseTR - 1.0/3.0*theta*vone;
    const double  isoTR     = iso_n;
    const ivector kineTR    = kine_n;

    // trial stress-like hardening variables
    double  Kp  =  Hiso*isoTR + (Yinf - Y0)*(1.0-exp(-Yexp*isoTR));
    double  qTR = -Kp;
    ivector QTR = -2.0/3.0*Hkin*kineTR;

    // trial deviatoric principal Kirchhoff strees -- Hencky model
    ivector tauTR = kappa*theta * vone + 2.0*mu* devepseTR;


    //---------------------------------------------------------------------------------------
    //                   check trial state and radial return
    //---------------------------------------------------------------------------------------
    ivector devepse_c;

    double phiTR = yieldFunction(tauTR, QTR, qTR);
    if (phiTR <= 0.0)
    {
        // elastic step: trial -> n+1
        kine_c    = kineTR;
        iso_c     = isoTR;
        devepse_c = devepseTR;
        dgamma    = 0.0;

        // factors for tangent
        tgs       = 1.0;
        tgdelta   = 1.0;
    }

    else
    {
        // deviatoric shifted stress
        const ivector devbetaTR = 2.0*mu*devepseTR - QTR;
        nubarTR = devbetaTR * (1.0/devbetaTR.norm());

        // plastic step : return mapping in principal stretches space
        dgamma = radialReturn(devbetaTR, isoTR);

        // correct the trial quantities
        devepse_c = devepseTR - dgamma * nubarTR;
        kine_c    = kineTR    - dgamma * nubarTR;
        //iso_c     = isoTR     + dgamma * SQ23;

        // factors for tangent
        double Kpp = Hiso + (Yinf-Y0)*Yexp*exp(-Yexp*iso_c);
        tgs     = 1.0 - 2.0*mu*dgamma/devbetaTR.norm();
        tgdelta = 1.0/(1.0 + (Kpp+Hkin)/(3.0*mu)) - (1.0-tgs);
    }

    tau = kappa * theta * vone + 2.0 * mu * devepse_c;

    // update elastic finger tensor
    ivector epse_c = devepse_c + 1.0/3.0*theta*vone;
    be_c.setZero();
    for (unsigned i=0; i<3; i++)
    {
        lambda2(i) = exp(2.0*epse_c[i]);
        be_c.addScaledVdyadicV( lambda2(i), nn[i] );
    }
}




double FLemaitreMP::variationalScale() const
{
    return theFLemaitreMaterial.isVariational() ? tempc/tempn : 1.0;
}




double FLemaitreMP::volumetricEnergy() const
{
    return 0.0;
}




double FLemaitreMP::volumetricStiffness() const
{
    double iJ = 1.0/Jc;
    double k = theFLemaitreMaterial.bulk;

    for (unsigned i=0; i<3; i++) k -= 2.0/9.0*tau(i);

    return k*iJ;
}




// yield function in principal Kirchhoff space
//
//      phi(tau, Q, q) = || dev(tau) - Qbar || - sqrt(2/3) (Yo - q)
//
//      tau : principal Kirchhoff stresses
//      Q   : kinematic hardening stress-like internal variable
//      q   : isotropic hardening stress-like internal variable

double FLemaitreMP::yieldFunction(const ivector& tau,
                                 const ivector& Q,
                                 const double&  q) const
{
    const double vScale = variationalScale();
    const double Y00   = theFLemaitreMaterial.Y0;
    const double soft  = theFLemaitreMaterial.soft;
    const double temp0 = theFLemaitreMaterial.referenceTemperature();
    const double Y0    = Y00*exp(-soft*(tempc-temp0)) * vScale;

    const ivector vone(1.0, 1.0, 1.0);
    const ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;

    //const double phi = (taubar - Q).norm() - SQ23*(Y0 - q);
    const double phi = (taubar - Q).norm() - (Y0 - q);
    return phi;
}
